﻿using SmartHomeAdmin.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartHomeAdmin
{
    public partial class AddDevice : Form
    {
        public House Home { get; set; }
        private int selIndex = -1;

        public AddDevice()
        {
            InitializeComponent();
        }

        public AddDevice(House h): this()
        {
            Home = h;
        }

        private void AddDevice_Load(object sender, EventArgs e)
        {
            lblHouseName.Text = Home.Name;
            lblHouseOwner.Text = Home.UserName;
            Application.DoEvents();
        }

        private void AddDevice_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to quit?", "Quiting application"
                , MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void AddDevice_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Dispose();
        }

        private void btnAddAction_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxName.Text) || string.IsNullOrEmpty(tbxDescription.Text) || string.IsNullOrEmpty(tbxMeasurementUnit.Text))
                return;
            Data.Action act = new Data.Action()
            {
                Id = "0",
                Name = tbxName.Text,
                Description = tbxDescription.Text,
                IsRepeatable = cbxRepeatable.Checked,
                MeasurementUnit = tbxMeasurementUnit.Text
            };
            lbxActions.DisplayMember = "Name";
            lbxActions.ValueMember = "Id";
            lbxActions.Items.Add(act);
            lbxActions.Refresh();
        }

        private void btnEditAction_Click(object sender, EventArgs e)
        {
            selIndex = lbxActions.SelectedIndex;
            if (selIndex == -1)
                return;
            Data.Action act = (Data.Action)lbxActions.SelectedItem;
            tbxName.Text = act.Name;
            tbxDescription.Text = act.Description;
            cbxRepeatable.Checked = act.IsRepeatable;
            tbxMeasurementUnit.Text = act.MeasurementUnit;
            lbxActions.Enabled = false;
            btnAddAction.Enabled = false;
            btnSave.Enabled = true;
            btnDeleteAction.Enabled = false;
        }

        private void btnDeleteAction_Click(object sender, EventArgs e)
        {
            if (lbxActions.SelectedIndex == -1)
                return;
            lbxActions.Items.RemoveAt(lbxActions.SelectedIndex);
            lbxActions.Refresh();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to cancel?"
                , "Cancel"
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                Form f = new EditHouse(Home);
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to submit?"
                , "Submitting device"
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                List<Data.Action> actions = new List<Data.Action>();
                foreach (var item in lbxActions.Items)
                    actions.Add((Data.Action)item);
                Device d = new Device()
                {
                    Actions = actions,
                    Name = tbxDeviceName.Text,
                    Type = tbxDeviceType.Text
                };
                Home.Devices.Add(d);
                Form f = new EditHouse(Home);
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            lbxActions.Enabled = true;
            lbxActions.SelectedIndex = selIndex;
            lbxActions.Items.RemoveAt(selIndex);
            Data.Action act = new Data.Action()
            {
                Id = "0",
                Name = tbxName.Text,
                Description = tbxDescription.Text,
                IsRepeatable = cbxRepeatable.Checked,
                MeasurementUnit = tbxMeasurementUnit.Text
            };
            //lbxActions.DisplayMember = "Name";
            //lbxActions.ValueMember = "Id";
            lbxActions.Items.Add(act);
            lbxActions.Refresh();
            btnAddAction.Enabled = true;
            btnDeleteAction.Enabled = true;
            btnSave.Enabled = false;
        }
    }
}
