﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilsAMQP.Models
{
    public class DTOCity : DTOBase
    {
        public string Name { get; set; }
        public string StateId { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }
    }
}
