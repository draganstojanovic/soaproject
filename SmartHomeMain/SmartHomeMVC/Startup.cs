﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Owin;
using SmartHomeMVC.Helpers;
using Microsoft.Extensions.DependencyInjection;
using System.Web.Mvc;
using System;
using System.Collections.Generic;

[assembly: OwinStartupAttribute(typeof(SmartHomeMVC.Startup))]
namespace SmartHomeMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}