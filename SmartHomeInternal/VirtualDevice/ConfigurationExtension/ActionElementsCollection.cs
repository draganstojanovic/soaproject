﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class ActionElementsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ActionElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ActionElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "action"; }
        }

        public ActionElement this[int index]
        {
            get { return (ActionElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public ActionElement this[string name]
        {
            get { return (ActionElement)BaseGet(name); }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }
    }
}
