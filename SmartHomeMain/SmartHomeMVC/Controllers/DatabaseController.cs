﻿using SmartHomeMVC.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilsAMQP.Models;

namespace SmartHomeMVC.Controllers
{
    public class DatabaseController : Controller
    {
        // GET: Database
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public string ProcessRequest(string messageModelJson)
        {
            MessageModel messageModel = MessageModel.DeserializeObject(messageModelJson);

            string jsonRet = MessageModel.SerializeObject(MessageHelper.ProcessMessageInstance(messageModel));
            return jsonRet;
        }
    }
}