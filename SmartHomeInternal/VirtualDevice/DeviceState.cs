﻿using DTOLayer;
using VirtualDevice.ConfigurationExtension;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;

namespace VirtualDevice
{
    /// <summary>
    /// <paramref name="connected" meaning=": device socket connection has been established;"/>
    /// <paramref name="on" meaning=": device is able to accept commands;"/>
    /// <paramref name="off" meaning=": device can not accept any command until it's on again;"/>
    /// <paramref name="disconnected" meaning=": device is terminated."/>
    /// </summary>
    public enum StateName : byte
    {
        on = 1,
        off = 2//,
        //connected = 4,
        //disconnected = 8
    }
    public abstract class DeviceState
    {
        public Device device { get; set; }
        public StateName NameOfState { get; private set; }
        /// <summary>
        /// list of actions that the device can do when it is on
        /// </summary>
        public static List<string> Actions { get; private set; }

        public DeviceState(StateName sn)
        {
            NameOfState = sn;
        }

        protected void LoadAndSetConfigActions()
        {
            try
            {
                List<string> ConfigActions = new List<string>();
                if (ConfigurationManager.GetSection(ConfigurationManager.AppSettings["DeviceSection"]) is DevicesSection ds)
                {
                    for (int i = 0; i < ds.DeviceTypes.Count; i++)
                    {
                        if (ds.DeviceTypes[i].Type.Equals(device.DeviceType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            DeviceTypeElement dType = ds.DeviceTypes[i];
                            int j;
                            for (j = 0; j < dType.Actions.Count; j++)
                            {
                                ConfigActions.Add(dType.Actions[j].Name);
                            }
                            for (j = 0; j < dType.Devices.Count; j++)
                            {
                                if (dType.Devices[j].Name.Equals(device.DeviceName, StringComparison.CurrentCultureIgnoreCase))
                                {
                                    DeviceElement dElem = dType.Devices[j];
                                    for (int k = 0; k < dElem.Actions.Count; k++)
                                    {
                                        ActionElement dAct = dElem.Actions[k];
                                        if (!ConfigActions.Contains(dAct.Name))
                                            ConfigActions.Add(dAct.Name);
                                    }
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
                Actions = ConfigActions;
            }
            catch (Exception e)
            {
                Console.WriteLine("Message:" + e.Message);
                Console.WriteLine("Inner Exception:" + e.InnerException);
                Console.WriteLine("Source" + e.Source);
                Console.WriteLine("StackTrace" + e.StackTrace);
                throw;
            }
        }

        public abstract DTOReturnLog Command(string methodName, object[] methodParams);
    }

    public class OffState : DeviceState
    {
        public OffState(Device d) : base(StateName.off)
        {
            device = d;
            try
            {
                LoadAndSetConfigActions();
            }
            catch (Exception e)
            {
                throw new Exception("Configuration has been corrupted. Please contact support for more informations.");
            }
        }

        public override DTOReturnLog Command(string methodName, object[] methodParams)
        {
            if (methodName.Equals("TurnOn", StringComparison.CurrentCultureIgnoreCase))
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(methodName);
                Console.ResetColor();
                device.DeviceState = new OnState(device);
                return new DTOReturnLog() { IsReturnLog = true, IsSensor = false, ReturnValue = "Succesfully turned on." };
            }
            else
                return new DTOReturnLog() { IsError = true, IsReturnLog = true, ReturnValue = "You can only turn on device when it is in off state." };
        }
    }

    public class OnState : DeviceState
    {
        //static da se samo jednom kreira u programu, a ne svaki put kad se OnState kreira ;)
        private static DeviceActions da;

        public OnState(Device d) : base(StateName.on)
        {
            device = d;
            da = DeviceActions.GetInstance;
            da.DevState = this;
        }

        public override DTOReturnLog Command(string methodName, object[] methodParams)
        {
            if (methodName.Equals("TurnOn", StringComparison.CurrentCultureIgnoreCase))
                return new DTOReturnLog() { IsError = true, IsReturnLog = true, IsSensor = false, ReturnValue = "Device is already on." };
            if (methodName.Equals("TurnOff", StringComparison.CurrentCultureIgnoreCase))
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.Beep();
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(methodName);
                Console.ResetColor();
                device.DeviceState = new OffState(device);
                return new DTOReturnLog() { IsReturnLog = true, IsSensor = false, ReturnValue = "Succesfully turned off." };
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Gray;
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(methodName);
                Console.ResetColor();
                Type t = da.GetType();
                MethodInfo mi;
                try
                {
                    mi = t.GetMethod(methodName);
                    if (Actions.Contains(mi.Name))
                    {
                        return (DTOReturnLog)mi.Invoke(da, methodParams);
                    }
                    else
                        return new DTOReturnLog() { IsError = true, ReturnValue = "This device does not have a method that you have just tried to call. Method name: " + mi.Name + "." };

                }
                catch (Exception e)
                {
                    Console.WriteLine("DeviceState.cs/OnState/Command");
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("SocketConnector.cs/Start");
                    Console.WriteLine("Message:" + e.Message);
                    Console.WriteLine("Inner Exception:" + e.InnerException);
                    Console.WriteLine("Source" + e.Source);
                    Console.WriteLine("StackTrace" + e.StackTrace);
                    Console.ResetColor();
                    mi = null;
                    return new DTOReturnLog() { IsError = true, ReturnValue = "Something unussual just happened. Try again or please contact support for more information." };
                }
            }
        }
    }
}
