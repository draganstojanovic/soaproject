﻿using DTOLayer;

namespace VirtualDevice
{
    public class Device
    {
        private static string deviceName;
        private static string deviceType;
        DeviceState state;
        public SocketConnector sc;
        public string DeviceName { get { return deviceName; } }
        public string DeviceType { get { return deviceType; } }
        public DeviceState DeviceState { get { return state; } set { state = value; } }

        public Device(string type, string name)
        {
            deviceName = name;
            deviceType = type;
            state = new OffState(this);
            sc = new SocketConnector(this);
        }

        public void Start()
        {
            sc.Start();
            sc.Stop();
        }

        public DTOReturnLog Command(DTOJob act)
        {
            return state.Command(act.MethodName, act.MethodParams);
        }
    }
}
