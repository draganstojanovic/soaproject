﻿namespace UtilsAMQP
{
    public abstract class AMQPBase
    {
        private string address = "localhost";
        private string port = "5672";        
        private string protocol = "amqp";
        private string user = "guest:guest";
        private string serverRequestProcessor = "default_processor";

        public string ServerRequestProcessorName { get => serverRequestProcessor; set => serverRequestProcessor = value; }
        public string Address { get => address; set => address = value; }
        public string Port { get => port; set => port = value; }
        public string Url
        {
            get
            {
                return Protocol + "://" + User + "@" + Address + ":" + Port;
            }
        }
        public string Protocol { get => protocol; set => protocol = value; }
        public string User { get => user; set => user = value; }

        public AMQPBase() { }
        public AMQPBase(string address, string port)
        {
            this.address = address; this.port = port;
        }
    }
}
