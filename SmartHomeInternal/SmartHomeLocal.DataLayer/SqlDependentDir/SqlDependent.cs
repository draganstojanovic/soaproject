﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeLocal.DataLayer.SqlDependentDir
{
    public abstract class SqlDependent
    {
        private string sqlQuery;// = "SELECT Id, Content FROM dbo.Table1";

        private string connString;// = "Server=ARXYA-WIN10X64L\\SQLEXPRESS;Database=testingCLR;Trusted_Connection=true";
        private string queueName;// = "queue1";
        private string serviceName;// = "service1";
        private string localDatabaseName;// = "testingCLR";

        private string dependentName;

        private bool _started = false;

        public string ConnString { get { return connString; } }
        public string QueueName { get { return queueName; } }
        public string ServiceName { get { return serviceName; } }
        public string LocalDatabaseName { get { return localDatabaseName; } }

        public string DependentName { get { return dependentName; } }

        public SqlDependent(string sqlQuery, string connString, string queueName, string serviceName, string localDatabaseName)
        {
            this.sqlQuery = sqlQuery;
            this.connString = connString;
            this.queueName = queueName;
            this.serviceName = serviceName;
            this.localDatabaseName = localDatabaseName;

            Random rnd = new Random((int)DateTime.Now.Ticks);
            this.dependentName = "random" + rnd.Next(1, 100);
        }

        public SqlDependent(string sqlQuery, string connString, string queueName, string serviceName, string localDatabaseName, string dependentName)
        {
            this.sqlQuery = sqlQuery;
            this.connString = connString;
            this.queueName = queueName;
            this.serviceName = serviceName;
            this.localDatabaseName = localDatabaseName;
            this.dependentName = dependentName;
        }

        public void Initialization()
        {
            // enabling broker, creating queue and service
            using (SqlConnection connection = new SqlConnection(connString))
            {
                connection.Open();

                //enabling broker
                {
                    string createQueueServiceQuery = "ALTER DATABASE " + localDatabaseName + " SET ENABLE_BROKER with rollback immediate";

                    using (SqlCommand command = new SqlCommand(createQueueServiceQuery, connection))
                    {
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception e) { }
                    }
                }

                //creating queue and service  -- possibly throws an exception (NOT)
                {
                    string createQueueServiceQuery = "CREATE QUEUE " + queueName + " " +
                                                        "CREATE SERVICE " + serviceName + " " +
                                                            "ON QUEUE " + queueName + "([http://schemas.microsoft.com/SQL/Notifications/PostQueryNotification])";

                    using (SqlCommand command = new SqlCommand(createQueueServiceQuery, connection))
                    {
                        try
                        {
                            command.ExecuteNonQuery();
                        }
                        catch (Exception/* e*/)
                        {
                            //if (!e.ToString().Contains("There is already an object named '" + queueName + "'")
                            //    && !e.ToString().Contains("There is already an object named '" + serviceName + "'"))
                            //    throw e;
                        }
                    }
                }

                //clearing queue
                {
                    string clearQueueQuery = "DECLARE item_cursor CURSOR LOCAL FAST_FORWARD FOR " +
                                                //"SELECT s.conversation_handle FROM clients.dbo.MySBQueueName s LEFT JOIN sys.conversation_endpoints e ON e.conversation_handle = s.conversation_handle WHERE e.conversation_handle IS NULL; " +
                                                //"--or completely clear it below" +
                                                "SELECT s.conversation_handle FROM " + localDatabaseName + ".dbo." + queueName + " s; " +
                                                "OPEN item_cursor " +
                                                "DECLARE @conversation UNIQUEIDENTIFIER " +
                                                "FETCH NEXT FROM item_cursor INTO @conversation " +
                                                "WHILE @@FETCH_STATUS = 0 " +
                                                "BEGIN " +
                                                    "END CONVERSATION @conversation WITH CLEANUP " +
                                                    "FETCH NEXT FROM item_cursor INTO @conversation " +
                                                "END";

                    using (SqlCommand command = new SqlCommand(clearQueueQuery, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }

            }

            // Create a dependency connection.  
            bool started = SqlDependency.Start(connString, queueName);
            if (!started)
                Console.WriteLine("SqlDependency NOT started successfully on {0}!", dependentName);
            else
                _started = true;
        }

        public void RegisterAgain()
        {
            // Assume connection is an open SqlConnection.  
            using (SqlConnection connection = new SqlConnection(connString))
            {
                //connection.Open();

                // Create a new SqlCommand object.  
                using (SqlCommand command = new SqlCommand(
                    sqlQuery,
                    connection))
                {

                    // Create a dependency and associate it with the SqlCommand.  
                    SqlDependency dependency = new SqlDependency(command, "service=" + serviceName + "; local database=" + localDatabaseName, Int32.MaxValue);
                    // Maintain the refence in a class member.  

                    // Subscribe to the SqlDependency event.  
                    dependency.OnChange += new
                       OnChangeEventHandler(OnDependencyChange);

                    connection.Open();

                    // Execute the command.  
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        // Process the DataReader. 
                        reader.Read();
                        //while (reader.Read())
                        //{
                        //    for (int i = 0; i < reader.FieldCount; i++)
                        //    {
                        //        Console.Write(reader.GetValue(i).ToString() + " ");
                        //    }
                        //    Console.WriteLine();
                        //}
                        //Console.WriteLine();

                        reader.Close();
                    }
                }
            }
        }

        // Handler method  
        public void OnDependencyChange(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency = sender as SqlDependency;

            dependency.OnChange -= OnDependencyChange;

            RegisterAgain();

            SpecificBehavior(sender, e);
        }

        public abstract void SpecificBehavior(object sender, SqlNotificationEventArgs e);

        public void Termination()
        {
            // Release the dependency.  
            SqlDependency.Stop(connString, queueName);
        }
    }
}

