﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class ActionElement : ConfigurationElement
    {
        private static ConfigurationPropertyCollection _propertyCollection;
        private static ConfigurationProperty _name;

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _propertyCollection; }
        }

        static ActionElement()
        {
            _name = new ConfigurationProperty("name", typeof(string), null, ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey);
            _propertyCollection = new ConfigurationPropertyCollection();
            _propertyCollection.Add(_name);
        }

        public string Name
        {
            get { return (string)base[_name]; }
        }
    }
}

