USE [testingJobSchedule]
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[sp_add_job_quick]
		@job = N'myjob19',
		@mycommand1 = N'insert into table1 ([Content], [Time]) VALUES (''arsa'', GETDATE())',
		@stepname1 = N'step19a',
		@databasename1 = N'testingJobSchedule',
		@mycommand2 = N'exec sp_delete_job @job_name = ''myjob19''',
		@stepname2 = N'step19b',
		@databasename2 = N'msdb',
		@servername = N'arxya-win10x64l',
		@startdate = N'20180813',
		@starttime = N'212000',
		@schedulename = N'schedule19'

SELECT	'Return Value' = @return_value

GO
