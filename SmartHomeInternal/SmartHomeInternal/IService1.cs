﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using DTOLayer;

namespace SmartHomeInternal
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract, WebGet(UriTemplate = "Devices", ResponseFormat = WebMessageFormat.Json)]
        List<DTODevice> GetDevices();

        [OperationContract, WebGet(UriTemplate = "DeviceInfo?id={id}", ResponseFormat = WebMessageFormat.Json)]
        DTODevice GetDeviceInfo(int id);

        [OperationContract, WebGet(UriTemplate = "JobInfo?id={id}", ResponseFormat = WebMessageFormat.Json)]
        DTOJob GetJobInfo(int id);

        [OperationContract, WebInvoke(ResponseFormat = WebMessageFormat.Json)]
        MessageToSend SendMessage(DTOJob ate);

        [OperationContract, WebGet(UriTemplate = "Login")]
        void CreateInstance();
    }

    [DataContract]
    public class MessageToSend
    {
        [DataMember]
        public bool IsError { get; set; }
        [DataMember]
        public string Body { get; set; }
    }
}
