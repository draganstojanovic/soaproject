﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTOLayer
{
    [DataContract]
    public class DTOAction
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int DeviceId { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public bool IsRepeatable { get; set; }
        [DataMember]
        public string MeasureUnit { get; set; }
        [DataMember]
        public string Name { get; set; }

        private List<DTOJob> jobList;

        [DataMember]
        public List<DTOJob> JobList
        {
            get
            {
                if (jobList == null)
                    jobList = new List<DTOJob>();
                return jobList;
            }
            set { jobList = value; }
        }
    }
}
