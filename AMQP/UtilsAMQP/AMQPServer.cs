﻿using Amqp.Listener;
using System;

namespace UtilsAMQP
{
    public class AMQPServer : AMQPBase
    {
        private ContainerHost host = null;

        private AMQPRequestProcessor msgProcessor = null;

        public AMQPServer() : base() { }
        public AMQPServer(string address, string port) : base(address, port) { }

        public bool Start(AMQPRequestProcessor rp)
        {
            if(rp == null)
            {
                Console.WriteLine("AMQPRequestProcessor object can not be null!");
                return false;
            }

            msgProcessor = rp;

            try
            {
                Uri addressUri = new Uri(this.Url);

                if (host != null)
                {
                    host.Close();
                    host = null;
                }

                host = new ContainerHost(new Uri[] { addressUri }, null, addressUri.UserInfo);
                host.Open();

                host.RegisterRequestProcessor(this.ServerRequestProcessorName, msgProcessor);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Stop()
        {
            try
            {
                if (host != null)
                {
                    host.Close();
                    host = null;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
