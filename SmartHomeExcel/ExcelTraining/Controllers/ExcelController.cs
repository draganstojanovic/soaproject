﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Runtime.InteropServices;
using System.Web.Http;
//using Microsoft.Office.Interop.Excel;
using ExcelTraining.Models;
using Newtonsoft.Json;
using System.IO;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.Data;
using System.Text;
using System.Web.Hosting;
using System.Net.Http.Headers;
using System.Web;

namespace ExcelTraining.Controllers
{
    public class ExcelController : ApiController
    {
        [HttpPost]
        //[Route("api/Excel/GetExcel")]
        public HttpResponseMessage CreateExcel([FromBody]ExcelTable jsonData)
        {

            HttpResponseMessage response;
            response = Request.CreateResponse(HttpStatusCode.OK);
            string filePath = @"C:\" + jsonData.fileName + ".xlsx";

            try
            {
                response.Content = new StringContent(filePath);

                using (SpreadsheetDocument document = SpreadsheetDocument.Create(filePath, SpreadsheetDocumentType.Workbook))
                {
                    WorkbookPart workbookPart = document.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    worksheetPart.Worksheet = new Worksheet();

                    Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());

                    Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "Report" };

                    sheets.Append(sheet);

                    workbookPart.Workbook.Save();

                    List<Employee> employees = Employees.EmployeesList;

                    SheetData sheetData = worksheetPart.Worksheet.AppendChild(new SheetData());

                    // Constructing header
                    Row row = new Row();

                    OpenXmlElement[] cols = new Cell[jsonData.columns.Count];
                    for (int i = 0; i < jsonData.columns.Count; i++)
                        cols[i] = ConstructCell(jsonData.columns[i], CellValues.String);
                    row.Append(cols);

                    // Insert the header row to the Sheet Data
                    sheetData.AppendChild(row);

                    for (int i = 0; i < jsonData.data.Count; i++)
                    {
                        OpenXmlElement[] datas = new Cell[jsonData.columns.Count];
                        Row row1 = new Row();

                        for (int j = 0; j < jsonData.columns.Count; j++)
                        {
                            datas[j] = ConstructCell(jsonData.data[i][j], CellValues.Number);
                        }
                        row1.Append(datas);
                        sheetData.AppendChild(row1);
                    }

                    worksheetPart.Worksheet.Save();
                }

                //string bookName = "Report.xlsx";
                //var dataBytes = File.ReadAllBytes(filePath);
 
            }
            catch(Exception ex)
            {
                response.Content = new StringContent(filePath + "\n" + ex.Message);
                return response;
            }
            response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }

        public HttpResponseMessage GetExcel()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            var fileStream = new FileStream("C:\\Report.xlsx", FileMode.Open);
            response.Content = new StreamContent(fileStream);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = "Report.xlsx";
            return response;
        }
    

        private Cell ConstructCell(string value, CellValues dataType)
        {
            return new Cell()
            {
                CellValue = new CellValue(value),
                DataType = new EnumValue<CellValues>(dataType)
            };
        }

    }

    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public decimal Salary { get; set; }
    }

    public sealed class Employees
    {
        static List<Employee> _employees;
        const int COUNT = 15;

        public static List<Employee> EmployeesList
        {
            private set { }
            get
            {
                return _employees;
            }
        }

        static Employees()
        {
            Initialize();
        }

        private static void Initialize()
        {
            _employees = new List<Employee>();

            Random random = new Random();

            for (int i = 0; i < COUNT; i++)
            {
                _employees.Add(new Employee()
                {
                    Id = i,
                    Name = "Employee " + i,
                    DOB = new DateTime(1999, 1, 1).AddMonths(i),
                    Salary = random.Next(100, 10000)
                });
            }
        }
    }

}



//HttpResponseMessage response;

//Application xlApp = new Microsoft.Office.Interop.Excel.Application();
//xlApp.DisplayAlerts = false;

//            if (xlApp == null)
//            {
//                response = Request.CreateResponse(HttpStatusCode.Conflict, "value");
//                response.Content = new StringContent("Creating excel file is currently not possible");
//                return response;  //409
//            }

//            Workbook xlWorkBook;
//Worksheet xlWorkSheet;
//object misValue = System.Reflection.Missing.Value;

//xlWorkBook = xlApp.Workbooks.Add(misValue);
//            xlWorkSheet = (Worksheet) xlWorkBook.Worksheets.get_Item(1);

//            for(int i=0; i<jsonData.columns.Count; i++)
//                xlWorkSheet.Cells[1, i + 1] = jsonData.columns[i];

//            for (int i = 0; i<jsonData.data.Count; i++)
//                for (int j = 0; j<jsonData.columns.Count; j++)
//                xlWorkSheet.Cells[i + 2, j + 1] = jsonData.data[i][j];

//            string filePath = "E:\\ExcelTrainingData\\" + jsonData.fileName + ".xls";
//xlWorkBook.SaveAs(filePath, XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
//            xlWorkBook.Close(true, misValue, misValue);
//            xlApp.Quit();

//            Marshal.ReleaseComObject(xlWorkSheet);
//            Marshal.ReleaseComObject(xlWorkBook);
//            Marshal.ReleaseComObject(xlApp);

//            string reqBook = "E:\\ExcelTrainingData\\" + jsonData.fileName + ".xls";
//string bookName = "sample.xls";
//var dataBytes = File.ReadAllBytes(reqBook);
//var dataStream = new MemoryStream(dataBytes);

//response = Request.CreateResponse(HttpStatusCode.OK);
//            response.Content = new StreamContent(dataStream);
//response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
//            response.Content.Headers.ContentDisposition.FileName = bookName;
//            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
//            return response;