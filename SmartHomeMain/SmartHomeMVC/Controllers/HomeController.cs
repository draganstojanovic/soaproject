﻿using SmartHomeMVC.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Device.Location;
using System.Threading;
using System.Net;

namespace SmartHomeMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            GeoCoordinateWatcher watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.Default);
            watcher.TryStart(false, TimeSpan.FromSeconds(0.5));
            Thread.Sleep(1000);
            double lat = watcher.Position.Location.Latitude;
            double lon = watcher.Position.Location.Longitude;

            var client = new WebClient();
            var content = client.DownloadString("http://localhost/SmartHomeWeather/api/Weather/CheckSubscription?mail="+User.Identity.Name);
            if (content.Contains("Success"))
                ViewBag.Subscribed = true;
            else if(content.Contains("Failed"))
                ViewBag.Subscribed = false;

            ViewBag.Lat = lat;
            ViewBag.Lon = lon;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}