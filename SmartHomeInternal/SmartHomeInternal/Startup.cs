﻿using Owin;
using SmartHomeInternal.SqlDependencyDir;

namespace SmartHomeInternal
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();

            JobDependent dep = new JobDependent();
            dep.Initialization();
            dep.RegisterAgain();
        }
    }
}