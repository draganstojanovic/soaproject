﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UtilsAMQP;
using UtilsAMQP.Models;

namespace TestConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //AMQPClient cl = new AMQPClient("localhost", "5672");
            if (args.Length == 0)
            {
                AMQPClient cl = new AMQPClient();
                cl.Start();

                //MessageModel response = (MessageModel)cl.SendRecv(new MessageModel() { MethodName = "GetAllHousesForUser", RequestResponse = new List<object>() { new DTOUser() { Id = "a1925c1e-b7cc-4c16-b992-ac920c4afefa" } } });

                //if(response.IsError)
                //{
                //    Console.WriteLine(response.ErrorDescription);
                //}
                //else
                //{
                //    List<object> houses = response.RequestResponse;
                //    foreach (object u in houses)
                //    {
                //        DTOHouse house = u as DTOHouse;
                //        Console.WriteLine(house.Name + " " + house.Ip + " " + house.UserName);
                //    }
                //}

                MessageModel pingResponse = (MessageModel)cl.SendRecv(new MessageModel() { IsPing = true });
                if (pingResponse.IsPing)
                    Console.WriteLine("Ping successful!");

                MessageModel response = (MessageModel)cl.SendRecv(new MessageModel()
                {
                    MethodName = "DeleteHouse",
                    RequestResponse = new List<object>() {
                        new DTOHouse()
                        {
                             Id = "5",
                             UserId = "a1925c1e-b7cc-4c16-b992-ac920c4afefa",
                             StateId = "196",
                             CityId = "5462",
                             Port = "80",
                             Name = "DTOHouseChanged",
                             Ip = "localhost",
                             DbName = "SmartHomeLocalDb",
                             Contact = "+381691553733",
                             Address = "Domska 69"
                        }
                    }
                });

                if (response.IsError)
                {
                    Console.WriteLine(response.ErrorDescription);
                }
                else
                {
                    List<object> houses = response.RequestResponse;
                    foreach (object u in houses)
                    {
                        bool? house = u as bool?;
                        if(house == true)
                            Console.WriteLine("Successfully deleted!");
                        else
                            Console.WriteLine("Failure deleting!");
                    }
                }
                Console.WriteLine("\nPress RETURN to exit...");
                Console.Read();
            }
            else
            {
                AMQPServer sr = new AMQPServer();
                sr.Start(new RequestProcessorInstance());

                Console.WriteLine("\nServer started . . .");
                Console.Read();
            }
        }
    }

    public class RequestProcessorInstance : AMQPRequestProcessor
    {
        public async Task<MessageModel> ProcessMessageInstanceAsync(MessageModel obj)
        {
            var values = new Dictionary<string, string>
            {
               { "messageModelJson", MessageModel.SerializeObject(obj) }
            };

            var content = new FormUrlEncodedContent(values);

            var server = ConfigurationManager.AppSettings["serverAddress"];

            var response = await client.PostAsync(server, content);

            var responseString = await response.Content.ReadAsStringAsync();

            MessageModel retMsg = MessageModel.DeserializeObject(responseString);

            return retMsg;
        }

        public override MessageModel ProcessMessageInstance(MessageModel obj)
        {
            Task<MessageModel> t = ProcessMessageInstanceAsync(obj);
            t.Wait();
            return t.Result;
        }
    }
}
