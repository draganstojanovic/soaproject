USE SmartHomeLocalDb;
GO

DROP PROCEDURE IF EXISTS [dbo].[sp_start_job_quick];
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE procedure [dbo].[sp_start_job_quick]
	@job nvarchar(128)
as
--Add a job
EXEC [msdb].[dbo].[sp_start_job]
    @job_name = @job;