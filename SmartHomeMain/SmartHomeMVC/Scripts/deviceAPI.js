﻿//{
//    sada: "sasda";
//    sadas: "sdasasd"
//}
//=>
//[{sada:"sasda"}, {sadas: "sdasasd"}]

var Action = function () {
    var deviceTypeAndName = "";
    var methodName = "";
    var methodParams = [];
    var jobId = -1;
    var time = null;
    var repeatTimeMinutes = null;
    var repeatTimeSeconds = null;

    var GetDeviceTypeAndName = () => {
        return deviceTypeAndName;
    }
    var SetDeviceTypeAndName = (devTN) => {
        deviceTypeAndName = devTN;
    }
    var GetMethodName = () => {
        return methodName;
    }
    var SetMethodName = (name) => {
        methodName = name;
    }
    var GetJobId = () => {
        return jobId;
    }
    var SetJobId = (jid) => {
        jobId = jid;
    }

    var GetParams = () => {
        return methodParams.length > 0 ? methodParams : null;
    }
    var GetParamsPretty = () => {
        //forming object methodParams from array of objects
        var params = {};
        for (var i = 0; i < methodParams.length; i++) {
            var obj = methodParams[i];
            var key = Object.keys(obj)[0];
            params[key] = obj[key];
        }
        return params;
    }
    var SetParams = (obj) => {
        //forming array of objects from object methodParams to send through network
        //params = [];
        //for (var key in obj) {
        //    var newObj = {};
        //    newObj[key] = obj[key];
        //    params.push(newObj);
        //}
        //methodParams = params.slice();
        jobId = obj["JobId"];
        time = obj["Time"];
        repeatTimeMinutes = obj["RepeatTimeMinutes"];
        repeatTimeSeconds = obj["RepeatTimeSeconds"];
    }
    var GetAction = () => {
        var action = {};
        //should make action object
        action["deviceTypeAndName"] = GetDeviceTypeAndName();
        action["methodName"] = GetMethodName();
        action["jobId"] = jobId;
        action["time"] = time;
        action["repeatTimeMinutes"] = repeatTimeMinutes;
        action["repeatTimeSeconds"] = repeatTimeSeconds;
        action["methodParams"] = GetParams();
        return action;
    }
    var GetActionPretty = () => {
        var action = {};
        //should make action object
        action["deviceTypeAndName"] = GetDeviceTypeAndName();
        action["methodName"] = GetMethodName();
        action["jobId"] = jobId;
        action["time"] = time;
        action["repeatTimeMinutes"] = repeatTimeMinutes;
        action["repeatTimeSeconds"] = repeatTimeSeconds;
        action["methodParams"] = GetParamsPretty();
        return action;
    }
    return {
        GetAction: GetAction,
        GetActionPretty: GetActionPretty,
        SetDeviceTypeAndName: SetDeviceTypeAndName,
        SetMethodName: SetMethodName,
        SetParams: SetParams
    }
}

//default success and error functions to be used with sendMsgToDevice method
var ActionSuccess = function (data) {
    console.log(data.IsError + " " + data.Body);
}

var ActionError = function (jqXHR, textStatus, error) {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(error);
}

jQuery.extend({
    sendMsgToDevice: function (action, success, error, targethost) {
        var par = JSON.stringify(action.GetAction());
        //var par = '"{"deviceTypeAndName":"Pecka: Alfa","methodName":"TurnOn","methodParams":[{"JobId":-1},{"Time":null},{"RepeatTimeMinutes":null},{"RepeatTimeSeconds":null}]}"';
        $.ajax({
            contentType: "application/json; charset=UTF-8",
            type: "POST", //same as method:'POST' ; type is for older versions
            method: "POST",
            url: "http://" + targethost + "/SmartHomeInternal/Service1.svc/SendMessage",
            data: par,
            dataType: "json",
            success: success,
            error: error
        });
    }

    //,

    ////sending message to arduino
    ////c should have some library for parsing json like json-c
    //sendMsgToOriginalDevice: function (action, success, error) {
    //    var methodParams = action.GetParams();
    //    // or should I: var methodParams = action.GetParams;
    //    var device = action.GetDeviceNameAndType();
    //    var method = action.GetMethodName();
    //    var newAction = (function (par, dev, met) {
    //        var obj = {};
    //        obj["deviceSignature"] = device;
    //        obj["methodName"] = method;
    //        for (var key in methodParams)
    //        {
    //            obj[key] = methodParams[key];
    //        }
    //        return obj;
    //    })(methodParams, device, method);
    //    $.ajax({
    //        contentType: "POST",
    //        url: "DeviceURL",
    //        data: JSON.stringify(newAction),
    //        dataType: "application/json; charset:UTF-8;",
    //        success: success,
    //        error: error
    //    });
    //}
});