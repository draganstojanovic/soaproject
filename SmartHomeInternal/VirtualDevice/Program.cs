﻿using System;
using System.Threading;

namespace VirtualDevice
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null)
                return;
            foreach (string arg in args)
                Console.WriteLine(arg);
            //type than name
            //Device d = new Device("Masina", "Beko");
            Device d = new Device(args[0], args[1]);
            d.Start();
            Exiting();
        }

        static private void Exiting()
        {
            Console.WriteLine("Device is closing in:");
            Console.WriteLine("3");
            Thread.Sleep(1000);
            Console.WriteLine("2");
            Thread.Sleep(1000);
            Console.WriteLine("1");
            Thread.Sleep(1000);
        }
    }
}
