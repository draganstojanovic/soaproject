use [testingJobSchedule];
go

exec [dbo].[sp_add_job_quick] 
@job = N'myjob19', -- The job name
@mycommand1 = N'insert into table1 ([Content], [Time]) VALUES (''arsa'', GETDATE())', -- The T-SQL command to run in the step
@mycommand2 = N'exec sp_delete_job @job_name = N''myjob19''', -- The T-SQL command to run in the step
@servername = N'arxya-win10x64l', -- SQL Server name. If running localy, you can use @servername=@@Servername
@startdate = '20180813', -- The date August 29th, 2013
@starttime = '212000' ,-- The time, 16:00:00
@stepname1 = N'step19a',
@stepname2 = N'step19b',
@schedulename = N'schedule19',
@databasename1 = N'testingJobSchedule',
@databasename2 = N'msdb'