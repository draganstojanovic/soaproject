﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class DeviceElement : ConfigurationElement
    {
        private static ConfigurationPropertyCollection _propertyCollection;
        private static ConfigurationProperty _name;
        private static ConfigurationProperty _actions;

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _propertyCollection; }
        }

        public string Name
        {
            get { return (string)base[_name]; }
            //set { this[_name] = value; }
        }

        public ActionElementsCollection Actions
        {
            get { return (ActionElementsCollection)base[_actions]; }
            //set { this[_actions] = value; }
        }



        static DeviceElement()
        {
            _name = new ConfigurationProperty("name", typeof(string), "", ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey);
            _actions = new ConfigurationProperty("actions", typeof(ActionElementsCollection), null, ConfigurationPropertyOptions.None);
            _propertyCollection = new ConfigurationPropertyCollection();
            _propertyCollection.Add(_name);
            _propertyCollection.Add(_actions);
        }
    }
}
