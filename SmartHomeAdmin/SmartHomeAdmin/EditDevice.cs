﻿using SmartHomeAdmin.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartHomeAdmin
{
    public partial class EditDevice : Form
    {
        public House Home { get; set; }
        public Device EditedDevice { get; set; }
        private int selIndex = -1;
        private int selDevIndex = -1;

        public EditDevice()
        {
            InitializeComponent();
        }

        public EditDevice(House h, Device d, int selectedDeviceIndex): this()
        {
            Home = h;
            lblHouseName.Text = h.Name;
            lblHouseOwner.Text = h.UserName;
            tbxDeviceType.Text = d.Type;
            tbxDeviceName.Text = d.Name;
            EditedDevice = d;
            lbxActions.ValueMember = "Id";
            lbxActions.DisplayMember = "Name";
            lbxActions.DataSource = null;
            lbxActions.DataSource = EditedDevice.Actions;
            selDevIndex = selectedDeviceIndex;
        }

        private void EditDevice_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("Idemo");
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to submit?"
                , "Submitting device"
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                List<Data.Action> actions = new List<Data.Action>();
                foreach (var item in lbxActions.Items)
                    actions.Add((Data.Action)item);
                Device d = new Device()
                {
                    Actions = actions,
                    Name = tbxDeviceName.Text,
                    Type = tbxDeviceType.Text
                };
                //edit
                if(selDevIndex!=-1)
                    Home.Devices.RemoveAt(selDevIndex);
                Home.Devices.Add(d);
                Form f = new EditHouse(Home);
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to cancel?"
                , "Cancel"
                , MessageBoxButtons.OKCancel
                , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                Form f = new EditHouse(Home);
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void btnAddAction_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbxName.Text) || string.IsNullOrEmpty(tbxDescription.Text) || string.IsNullOrEmpty(tbxMeasurementUnit.Text))
                return;
            Data.Action act = new Data.Action()
            {
                 Id = "0",
                 Name = tbxName.Text,
                 Description = tbxDescription.Text,
                 IsRepeatable = cbxRepeatable.Checked,
                 MeasurementUnit = tbxMeasurementUnit.Text
            };
            EditedDevice.Actions.Add(act);
            lbxActions.Refresh();
        }

        private void btnEditAction_Click(object sender, EventArgs e)
        {
            selIndex = lbxActions.SelectedIndex;
            if (selIndex == -1)
                return;
            Data.Action act = (Data.Action)lbxActions.SelectedItem;
            tbxName.Text = act.Name;
            tbxDescription.Text = act.Description;
            cbxRepeatable.Checked = act.IsRepeatable;
            tbxMeasurementUnit.Text = act.MeasurementUnit;
            lbxActions.Enabled = false;
            btnAddAction.Enabled = false;
            btnSave.Enabled = true;
            btnDeleteAction.Enabled = false;
        }

        private void btnDeleteAction_Click(object sender, EventArgs e)
        {
            if (lbxActions.SelectedIndex == -1)
                return;
            //lbxActions.Items.RemoveAt(lbxActions.SelectedIndex);
            EditedDevice.Actions.RemoveAt(lbxActions.SelectedIndex);
            lbxActions.DataSource = null;
            lbxActions.DisplayMember = "Name";
            lbxActions.ValueMember = "Id";
            lbxActions.DataSource = EditedDevice.Actions;
            lbxActions.Refresh();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            lbxActions.Enabled = true;
            lbxActions.SelectedIndex = selIndex;
            EditedDevice.Actions.RemoveAt(selIndex);
            Data.Action act = new Data.Action()
            {
                Id = "0",
                Name = tbxName.Text,
                Description = tbxDescription.Text,
                IsRepeatable = cbxRepeatable.Checked,
                MeasurementUnit = tbxMeasurementUnit.Text
            };
            EditedDevice.Actions.Add(act);
            lbxActions.DataSource = null;
            lbxActions.DisplayMember = "Name";
            lbxActions.ValueMember = "Id";
            lbxActions.DataSource = EditedDevice.Actions;
            lbxActions.Refresh();
            btnAddAction.Enabled = true;
            btnDeleteAction.Enabled = true;
            btnSave.Enabled = false;
        }
    }
}
