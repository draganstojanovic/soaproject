﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeAdmin.Data
{
    public class City
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }

        public override bool Equals(object obj)
        {
            var city = obj as City;
            return city != null &&
                   Id == city.Id &&
                   Name == city.Name;
        }
    }

    public class Cities
    {
        public List<City> AllCities { get; set; }
    }
}
