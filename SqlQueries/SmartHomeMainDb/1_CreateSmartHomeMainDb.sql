CREATE DATABASE [SmartHomeMainDb]
GO

USE [SmartHomeMainDb]
GO

CREATE TABLE [User] (
    Id			nvarchar(128)			IDENTITY(1,1) PRIMARY KEY,
    Username	nvarchar(255)	NOT NULL,
    Password	nvarchar(255)	NOT NULL,
    Role		nvarchar(255)	NOT NULL,
    Mail		nvarchar(255)	NOT NULL,
	Phone		nvarchar(255)	NOT NULL
);

CREATE TABLE [States] (
    Id			int				IDENTITY(1,1) PRIMARY KEY,
    Name		nvarchar(255)	NOT NULL,
    Abbrevation	nvarchar(255)	NOT NULL
);

CREATE TABLE [Cities] (
    Id			int				IDENTITY(1,1) PRIMARY KEY,
    Name		nvarchar(255)	NOT NULL,
    Latitude	float			NOT NULL,
    Longitude	float			NOT NULL,
	StateId		int,
	FOREIGN KEY (StateId) REFERENCES [States](Id)
);

CREATE TABLE [Houses] (
    Id			int			IDENTITY(1,1) PRIMARY KEY,
    Ip			nvarchar(255)	NOT NULL,
    Port		nvarchar(255)	NOT NULL,
    DbName		nvarchar(255)	NOT NULL,
	Name		nvarchar(255)	NOT NULL,
	Address		nvarchar(255)	NOT NULL,
	Contact		nvarchar(255)	NOT NULL,
    UserId		nvarchar(128)			NOT NULL,
	CityId		int			NOT NULL,
	StateId		int			NOT NULL,
	FOREIGN KEY (UserId) REFERENCES [User](Id),
	FOREIGN KEY (CityId) REFERENCES [Cities](Id),
	FOREIGN KEY (StateId) REFERENCES [States](Id)
);

