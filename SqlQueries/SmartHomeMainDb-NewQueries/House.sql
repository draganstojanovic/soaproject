USE [SmartHomeMainDb]
GO

/****** Object:  Table [dbo].[Houses]    Script Date: 8/15/2018 10:44:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Houses](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ip] [nvarchar](255) NOT NULL,
	[Port] [nvarchar](255) NOT NULL,
	[DbName] [nvarchar](255) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Address] [nvarchar](255) NOT NULL,
	[Contact] [nvarchar](255) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[CityId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Houses]  WITH CHECK ADD FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([Id])
GO

ALTER TABLE [dbo].[Houses]  WITH CHECK ADD FOREIGN KEY([StateId])
REFERENCES [dbo].[States] ([Id])
GO

ALTER TABLE [dbo].[Houses]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO

