﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DTOLayer
{
    [DataContract]
    public class DTODevice
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public DateTime DateOfReg { get; set; }

        [DataMember]
        public string DateOfRegStr { get; set; }
        [DataMember]
        public string URL { get; set; }
        [DataMember]
        public string Type { get; set; }

        private List<DTOAction> actionList;

        [DataMember]
        public List<DTOAction> ActionList
        {
            get
            {
                if (actionList == null)
                    actionList = new List<DTOAction>();
                return actionList;
            }
            set { actionList = value; }
        }
    }
}
