﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class DeviceTypeElement : ConfigurationElement
    {
        private static ConfigurationPropertyCollection _propertyCollection;
        private static ConfigurationProperty _type;
        private static ConfigurationProperty _devices;
        private static ConfigurationProperty _actions;

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _propertyCollection; }
        }

        public string Type
        {
            get { return (string)base[_type]; }
            //set { this[_type] = value; }
        }

        public DeviceElementsCollection Devices
        {
            get { return (DeviceElementsCollection)base[_devices]; }
            //set { this[_devices] = value; }
        }

        public ActionElementsCollection Actions
        {
            get { return (ActionElementsCollection)base[_actions]; }
            //set { this[_actions] = value; }
        }

        static DeviceTypeElement()
        {
            _type = new ConfigurationProperty("type", typeof(string), "", ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey);
            _devices = new ConfigurationProperty("devices", typeof(DeviceElementsCollection), null, ConfigurationPropertyOptions.None);
            _actions = new ConfigurationProperty("actions", typeof(ActionElementsCollection), null, ConfigurationPropertyOptions.None);

            _propertyCollection = new ConfigurationPropertyCollection();
            _propertyCollection.Add(_type);
            _propertyCollection.Add(_devices);
            _propertyCollection.Add(_actions);
        }
    }
}
