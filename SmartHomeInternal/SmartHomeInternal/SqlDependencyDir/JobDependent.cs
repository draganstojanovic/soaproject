﻿using DTOLayer;
using Microsoft.AspNet.SignalR;
using SmartHomeInternal.Models;
using SmartHomeInternal.SignalRHubs;
using SmartHomeLocal.DataLayer;
using SmartHomeLocal.DataLayer.SqlDependentDir;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;

namespace SmartHomeInternal.SqlDependencyDir
{
    public class JobDependent : SqlDependent
    {
        public JobDependent(string sqlQuery, string connString, string queueName, string serviceName, string localDatabaseName)
            : base(sqlQuery, connString, queueName, serviceName, localDatabaseName) { }

        public JobDependent()
            : base("SELECT [Id], [IsReady] FROM [dbo].[Job]",
                  ConfigurationManager.ConnectionStrings["SmartHomeLocalDbEntitiesClean"].ConnectionString,
                  "jobsQueue",
                  "jobsService",
                  "SmartHomeLocalDb")
        { }

        public override void SpecificBehavior(object sender, SqlNotificationEventArgs e)
        {
            if (e.Info != SqlNotificationInfo.Update)
                return;

            using (var data = new SmartHomeLocalDbEntities())
            {
                var readyJobs = data.Job.Where(x => x.IsReady == true && x.IsStarted == false).ToList();

                foreach (Job j in readyJobs)
                {
                    // inform the device about the action etc.
                    DTOJob ate = new DTOJob()
                    {
                        DeviceTypeAndName = j.Action.Device.Type + ":" + j.Action.Device.Name,
                        MethodName = j.Action.Name,
                        //MethodParams = new object[]
                        //{
                        JobId = j.Id,
                          Time = j.Time,
                           RepeatTimeMinutes= j.RepeatTimeMinutes,
                           RepeatTimeSeconds= j.RepeatTimeSeconds
                        //}
                    };

                    byte[] buffer = Service1.SerializeObject(ate);
                    int sendBytes = Service1.Devices[ate.DeviceTypeAndName].Send(buffer);

                    // change started flag in db
                    j.IsStarted = true;
                    data.SaveChanges();
                    // inform the user something if needed
                    var context = GlobalHost.ConnectionManager.GetHubContext<UniversalHub>();
                    //context.Clients.All.jobStarted(new JobModel()
                    //{
                    //    Id = j.Id,
                    //    ActionId = j.ActionId,
                    //    DeviceName = j.Action.Device.Name,
                    //    Time = j.Time,
                    //    RepeatTimeMinutes = j.RepeatTimeMinutes,
                    //    RepeatTimeSeconds = j.RepeatTimeSeconds,
                    //    IsSensor = j.IsSensor,
                    //    IsReady = j.IsReady,
                    //    IsStarted = j.IsStarted,
                    //    IsFinished = j.IsFinished ?? false
                    //});
                    context.Clients.All.jobStarted();
                }
            }
        }
    }
}