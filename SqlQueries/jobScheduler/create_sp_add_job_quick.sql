USE testingJobSchedule;
GO

DROP PROCEDURE IF EXISTS [dbo].[sp_add_job_quick];
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE procedure [dbo].[sp_add_job_quick]
	@job nvarchar(128),
	@mycommand1 nvarchar(max), 
	@stepname1 nvarchar(30),
	@databasename1 nvarchar(50),
	@mycommand2 nvarchar(max), 
	@stepname2 nvarchar(30),
	@databasename2 nvarchar(50),
	@servername nvarchar(28),
	@startdate nvarchar(8),
	@starttime nvarchar(8),
	@schedulename nvarchar(30)
as
--Add a job
EXEC msdb.dbo.sp_add_job
    @job_name = @job ;
--Add a job step named process step. This step runs the stored procedure
EXEC msdb.dbo.sp_add_jobstep
    @job_name = @job,
    @step_name = @stepname1,
    @subsystem = N'TSQL',
    @command = @mycommand1,
	@database_name = @databasename1,
	@on_success_action = 3
EXEC msdb.dbo.sp_add_jobstep
    @job_name = @job,
    @step_name = @stepname2,
    @subsystem = N'TSQL',
    @command = @mycommand2,
	@database_name = @databasename2;
--Schedule the job at a specified date and time
exec msdb.dbo.sp_add_jobschedule 
	@job_name = @job,
	@name = @schedulename,
	@freq_type=1,
	@active_start_date = @startdate,
	@active_start_time = @starttime;
-- Add the job to the SQL Server Server
EXEC msdb.dbo.sp_add_jobserver
    @job_name =  @job,
    @server_name = @servername;
RETURN SELECT 'arsaTest'