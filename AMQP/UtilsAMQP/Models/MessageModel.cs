﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace UtilsAMQP.Models
{
    public class MessageModel
    {
        public bool IsPing { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public bool IsResponse { get; set; }
        public string MethodName { get; set; }
        public List<object> RequestResponse { get; set; }
        public bool IsLogInMessage { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public MessageModel(MessageModel old)
        {
            IsError = old.IsError;
            ErrorDescription = old.ErrorDescription;
            IsResponse = old.IsResponse;
            MethodName = old.MethodName;
            RequestResponse = old.RequestResponse == null ? new List<object>() : new List<object>(old.RequestResponse);
            IsLogInMessage = old.IsLogInMessage;
            Email = old.Email;
            Password = old.Password;
        }

        public MessageModel() { RequestResponse = new List<object>(); }
        
        public static string SerializeObject(MessageModel rl)
        {
            try
            {
                var settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Objects;
                if (rl.RequestResponse == null)
                    rl.RequestResponse = new List<object>();
                string jsonObj = JsonConvert.SerializeObject(rl, settings);

                return jsonObj;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                return SerializeObject(new MessageModel() { IsError = true, ErrorDescription = "Exception while serializing object!" });
            }
        }

        public static MessageModel DeserializeObject(string jsonObj)
        {
            try
            {
                var settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Objects;
                MessageModel deSer = JsonConvert.DeserializeObject<MessageModel>(jsonObj, settings);
                return deSer;
            }
            catch (Exception e)
            {
                return new MessageModel() { IsError = true, ErrorDescription = "Exception while deserializing object!\n" + e.ToString() };
            }
        }
    }
}
