﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartHomeMVC.Models;

namespace SmartHomeMVC.Controllers
{
    public class CustomerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Customer
        public ActionResult IndexCustomers()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var customerRole = roleManager.FindByName("Customer").Users.First();
            var customers = db.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(customerRole.RoleId)).ToList();
            return View(customers);
        }

        //GET: Admin
        [Authorize(Roles = "Administrator")]
        public ActionResult IndexAdmins()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var adminRole = roleManager.FindByName("Administrator").Users.First();
            var admins = db.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(adminRole.RoleId)).ToList();
            return View(admins);
        }

        // POST: Customer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(applicationUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationUser);
        }

        // GET: Customer/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser appUser = db.Users.Find(id);
            if (appUser == null)
            {
                return HttpNotFound();
            }

            var user = new CustomerViewModel(appUser.Id, appUser.Email, appUser.PhoneNumber, "Not approved");
            List<IdentityUserRole> roles=(List<IdentityUserRole>)appUser.Roles;
            if (roles.First().RoleId=="1")
                user.RoleName = "Administrator";
            else
                user.RoleName = "Customer";

            return View(user);
        }

        // POST: Customer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,PhoneNumber,RoleName")] CustomerViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(applicationUser.Id);
                user.PhoneNumber = applicationUser.PhoneNumber;
                db.SaveChanges();
                if(applicationUser.RoleName=="Administrator")
                    return RedirectToAction("IndexAdmins");
                else
                    return RedirectToAction("IndexCustomers");
            }
            return View(applicationUser);
        }

        // GET: Customer/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: Customer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
