﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelTraining.Models
{
    public class ExcelTable
    {
        public string fileName { get; set; }
        public List<string> columns { get; set; }
        public List<List<string>> data { get; set; }
    }
}