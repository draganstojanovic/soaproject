﻿using DTOLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DbServiceLibrary
{
    public class Service1 : IService1
    {
        public MessageToSend AddDevices(List<DTODevice> devices)
        {
            try
            {
                //sacuvaj u bazu devices i actions
                string devsSignature = AddDevicesToDB(devices);
                MessageToSend mts = new MessageToSend
                {
                    IsError = false,
                    Body = devsSignature
                };
                return mts;
            }
            catch (Exception e)
            {
                return new MessageToSend() { IsError = true, Body = e.Message };
            }
        }

        public MessageToSend UpdateDevices(List<DTODevice> devices)
        {
            //update device
            //truncate actions for device, than save them
            try
            {
                UpdateDevicesInDB(devices);
                string dev = "device";
                if (devices.Count > 1)
                    dev = "devices";
                return new MessageToSend() { IsError = false, Body = "You have successfully updated " + dev };
            }
            catch (Exception e)
            {
                return new MessageToSend() { IsError = true, Body = e.Message + "\n" + e.InnerException };
            }
        }

        public MessageToSend DeleteDevices(List<DTODevice> devices)
        {
            try
            {
                DeleteDevicesFromDB(devices);
                string dev = "device";
                if (devices.Count > 1)
                    dev = "devices";
                return new MessageToSend() { IsError = false, Body = "You have successfully deleted " + dev };
            }
            catch (Exception e)
            {
                return new MessageToSend() { IsError = true, Body = e.Message };
            }
        }

        public List<DTODevice> GetAllDevices(string idHouse)
        {
            List<DTODevice> devices = new List<DTODevice>();

            using (var data = new SmartHomeLocalDbEntities())
            {
                List<Device> devs = data.Devices.Where(x => x.HouseId.Equals(idHouse)).ToList();
                foreach (var d in devs)
                {
                    List<DTOAction> dacts = new List<DTOAction>();
                    foreach (var a in d.Actions)
                    {
                        DTOAction act = new DTOAction()
                        {
                            Description = a.Description,
                            DeviceId = a.DeviceId,
                            Id = 0,
                            IsRepeatable = a.IsRepeatable ?? false,
                            MeasureUnit = a.MeasureUnit,
                            Name = a.Name
                        };
                        dacts.Add(act);
                    }
                    DTODevice dto = new DTODevice()
                    {
                        Id = d.Id,
                        Name = d.Name,
                        ActionList = dacts,
                        DateOfReg = d.DateOfReg,
                        Type = d.Type,
                        URL = d.URL
                    };
                    devices.Add(dto);
                }
            }
            return devices;
        }

        private string AddDevicesToDB(List<DTODevice> devices)
        {
            //type-name-id
            List<string> devicesSignature = new List<string>();
            using (var data = new SmartHomeLocalDbEntities())
            {
                foreach (var dev in devices)
                {
                    Device d = new Device()
                    {
                        DateOfReg = DateTime.UtcNow,
                        Name = dev.Name,
                        Type = dev.Type,
                        URL = ""
                    };
                    List<Action> actions = new List<Action>();
                    foreach (var act in dev.ActionList)
                    {
                        Action a = new Action()
                        {
                            Name = act.Name,
                            IsRepeatable = act.IsRepeatable,
                            MeasureUnit = act.MeasureUnit,
                            Description = act.Description,
                            Device = d
                        };
                        actions.Add(a);
                        data.Actions.Add(a);
                    }
                    d.Actions = actions;
                    data.Devices.Add(d);
                    data.SaveChanges();
                    devicesSignature.Add(d.Type + "-" + d.Name + "-" + d.Id);
                }
            }
            string tni = "";
            for (int i = 0; i < devicesSignature.Count; i++)
            {
                if (i == 0)
                {
                    tni = devicesSignature[i];
                }
                else
                {
                    tni += string.Concat(":", devicesSignature[i]);
                }
            }
            //type-name-id:type-name-id...
            return tni;
        }


        private void UpdateDevicesInDB(List<DTODevice> devices)
        {
            using (var data = new SmartHomeLocalDbEntities())
            {
                foreach (var dev in devices)
                {
                    Device d = data.Devices.Where(x => x.Id == dev.Id).FirstOrDefault();
                    if (d == null)
                    {
                        d = new Device()
                        {
                            Name = dev.Name,
                            Type = dev.Type,
                            DateOfReg = DateTime.UtcNow,
                            URL = ""
                        };
                        foreach (var act in dev.ActionList)
                        {
                            Action a = new Action()
                            {
                                Name = act.Name,
                                IsRepeatable = act.IsRepeatable,
                                MeasureUnit = act.MeasureUnit,
                                Description = act.Description,
                                Device = d
                            };
                            data.Actions.Add(a);
                        }
                        data.Devices.Add(d);
                    }
                    else
                    {
                        d.Name = dev.Name;
                        d.Type = dev.Type;
                        data.Actions.RemoveRange(d.Actions);
                        List<Action> actions = new List<Action>();
                        foreach (var act in dev.ActionList)
                        {
                            Action a = new Action()
                            {
                                Name = act.Name,
                                IsRepeatable = act.IsRepeatable,
                                MeasureUnit = act.MeasureUnit,
                                Description = act.Description,
                                Device = d
                            };
                            actions.Add(a);
                            data.Actions.Add(a);
                        }
                        d.Actions = actions;
                    }
                    data.SaveChanges();
                }
            }
        }

        private void DeleteDevicesFromDB(List<DTODevice> devices)
        {
            using (var data = new SmartHomeLocalDbEntities())
            {
                foreach (var dev in devices)
                {
                    Device d = data.Devices.Where(x => x.Id == dev.Id).FirstOrDefault();
                    if (d == null)
                        continue;
                    data.Actions.RemoveRange(d.Actions);
                    data.Devices.Remove(d);
                    data.SaveChanges();
                }
            }
        }
    }
}
