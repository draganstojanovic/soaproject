﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SmartHomeMVC.Models;

namespace SmartHomeMVC.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class NewCustomerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: NewCustomer
        public ActionResult Index()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var adminRole = roleManager.FindByName("Administrator").Users.First();
            var admins = db.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(adminRole.RoleId)).ToList();

            var customerRole = roleManager.FindByName("Customer").Users.First();
            var customers = db.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(customerRole.RoleId)).ToList();

            var users = db.Users.ToList();
            IList<CustomerViewModel> newCustomers = new List<CustomerViewModel>();
            foreach(ApplicationUser user in users)
            {
                if (!admins.Contains(user) && !customers.Contains(user))
                    newCustomers.Add(new CustomerViewModel(user.Id, user.Email, user.PhoneNumber, "Not approved"));
            }

            return View(newCustomers);
        }

        // GET: NewCustomer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewCustomer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName")] ApplicationUser applicationUser)
        {
            if (ModelState.IsValid)
            {
                db.Users.Add(applicationUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(applicationUser);
        }

        // GET: NewCustomer/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = db.Users.Find(id);
            var customer=new CustomerViewModel(user.Id, user.Email, user.PhoneNumber, "Not approved");
            if (user.Roles.Count() == 1)
                customer.RoleName = "Customer";
            if (user == null)
            {
                return HttpNotFound();
            }
            var context = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            List<RoleModel> rm = new List<RoleModel>();
            List<IdentityRole> roles = roleMngr.Roles.ToList();
            foreach (IdentityRole role in roles)
                rm.Add(new RoleModel(role.Id, role.Name));
            ViewBag.Roles = rm;
            return View(customer);
        }

        // POST: NewCustomer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,PhoneNumber,RoleName")] CustomerViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.Find(applicationUser.Id);
                user.PhoneNumber = applicationUser.PhoneNumber;
                var context = new ApplicationDbContext();
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                userManager.AddToRole(applicationUser.Id, applicationUser.RoleName);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(applicationUser);
        }

        // GET: NewCustomer/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = db.Users.Find(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // POST: NewCustomer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
