﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartHomeMVC.Models
{
    public class CustomerViewModel
    {
        public string Id { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Privileges")]
        public string RoleName { get; set; }

        public CustomerViewModel(string id, string email, string phoneNumber, string roleName)
        {
            Id = id;
            Email = email;
            PhoneNumber = phoneNumber;
            RoleName = roleName;
        }

        public CustomerViewModel()
        {
        }
    }
}