﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp1.ServiceReference1;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Service1Client test = new Service1Client();

            List<DTODevice> devs = new List<DTODevice>();
            DTOAction a1 = new DTOAction()
            {
                 Name = "sasd",
                  Description = "a1",
                   IsRepeatable = true,
                    MeasureUnit = "f"
            };
            DTOAction a2 = new DTOAction()
            {
                Name = "action2 upali",
                Description = "a2",
                IsRepeatable = true,
                MeasureUnit = "d"
            };
            List<DTOAction> al1 = new List<DTOAction>();
            al1.Add(a1);
            al1.Add(a2);
            List<DTOAction> al2 = new List<DTOAction>();
            DTOAction a3 = new DTOAction()
            {
                Name = "ugasi",
                Description = "a3",
                IsRepeatable = true,
                MeasureUnit = "c"
            };
            al2.Add(a3);
            DTODevice d1 = new DTODevice()
            {
                Id = 6,
                Name = "d1",
                Type = "devT",
                 DateOfReg = DateTime.Now,
            };
            d1.ActionList = al1.ToArray();
            DTODevice d2 = new DTODevice()
            {
                Id = 7,
                Name = "d2",
                Type = "deviceType",
                DateOfReg = DateTime.Now
            };
            d2.ActionList = al2.ToArray();
            devs.Add(d1);
            devs.Add(d2);

            //MessageToSend mts = test.AddDevices(devs.ToArray());
            //Console.WriteLine(mts.IsError);
            //Console.WriteLine(mts.Body);

            //MessageToSend mts = test.DeleteDevices(devs.ToArray());
            //Console.WriteLine(mts.IsError);
            //Console.WriteLine(mts.Body);

            MessageToSend mts = test.UpdateDevices(devs.ToArray());
            Console.WriteLine(mts.IsError);
            Console.WriteLine(mts.Body);
        }
    }
}
