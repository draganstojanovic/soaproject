CREATE DATABASE [SmartHomeLocalDb];
GO

USE [SmartHomeLocalDb];
GO

CREATE TABLE [Device] (
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[Name]	nvarchar(255) NOT NULL,
	[DateOfReg] datetime NOT NULL,
	[URL] nvarchar(255) NOT NULL,
	[Type] nvarchar(100)
);

CREATE TABLE [Action] (
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[DeviceId] int NOT NULL,
	[Name] nvarchar(100),
	[Description] nvarchar(255),
	[IsRepeatable] bit,
	[MeasureUnit] nvarchar(100),
	FOREIGN KEY ([DeviceId]) REFERENCES [Device]([Id])
);

CREATE TABLE [Job] (
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[ActionId]	int NOT NULL,
	[Time] datetime,
	[RepeatTimeMinutes] int,
	[RepeatTimeSeconds] int,
	[IsFinished] bit,
	[IsSensor] bit NOT NULL DEFAULT 0,
	[IsReady] bit NOT NULL DEFAULT 0,
	[IsStarted] bit NOT NULL DEFAULT 0,
	FOREIGN KEY ([ActionId]) REFERENCES [Action]([Id])
);

CREATE TABLE [ReturnLog] (
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[Time] datetime NOT NULL,
	[Value] nvarchar(255) NOT NULL,
	[ValueType] nvarchar(255) NOT NULL,
	[JobId] int NOT NULL,
	[IsSensor] bit,
	FOREIGN KEY ([JobId]) REFERENCES [Job]([Id])
);