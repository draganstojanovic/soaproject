﻿namespace SmartHomeAdmin
{
    partial class IndexFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxUsers = new System.Windows.Forms.ListBox();
            this.lbxHouses = new System.Windows.Forms.ListBox();
            this.lbllblUsername = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lbllblEmail = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lbllblPhoneNumber = new System.Windows.Forms.Label();
            this.lblPhoneNumber = new System.Windows.Forms.Label();
            this.lblUsers = new System.Windows.Forms.Label();
            this.lblHouses = new System.Windows.Forms.Label();
            this.lbllblName = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lbllblServer = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.lbllblDbName = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lbllblAddress = new System.Windows.Forms.Label();
            this.lblDbName = new System.Windows.Forms.Label();
            this.lbllblState = new System.Windows.Forms.Label();
            this.lbllblCity = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.lbllblContact = new System.Windows.Forms.Label();
            this.lblContact = new System.Windows.Forms.Label();
            this.lblUserInfo = new System.Windows.Forms.Label();
            this.lblHouseInfo = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addHouseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editHouseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.deleteHouseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbxUsers
            // 
            this.lbxUsers.FormattingEnabled = true;
            this.lbxUsers.Location = new System.Drawing.Point(12, 75);
            this.lbxUsers.Name = "lbxUsers";
            this.lbxUsers.Size = new System.Drawing.Size(211, 368);
            this.lbxUsers.TabIndex = 0;
            this.lbxUsers.SelectedIndexChanged += new System.EventHandler(this.lbxUsers_SelectedIndexChanged);
            // 
            // lbxHouses
            // 
            this.lbxHouses.FormattingEnabled = true;
            this.lbxHouses.Location = new System.Drawing.Point(243, 270);
            this.lbxHouses.Name = "lbxHouses";
            this.lbxHouses.Size = new System.Drawing.Size(215, 173);
            this.lbxHouses.TabIndex = 1;
            this.lbxHouses.SelectedIndexChanged += new System.EventHandler(this.lbxHouses_SelectedIndexChanged);
            // 
            // lbllblUsername
            // 
            this.lbllblUsername.AutoSize = true;
            this.lbllblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblUsername.Location = new System.Drawing.Point(241, 75);
            this.lbllblUsername.Name = "lbllblUsername";
            this.lbllblUsername.Size = new System.Drawing.Size(96, 20);
            this.lbllblUsername.TabIndex = 2;
            this.lbllblUsername.Text = "Username:";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(242, 95);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(60, 24);
            this.lblUsername.TabIndex = 3;
            this.lblUsername.Text = "label2";
            // 
            // lbllblEmail
            // 
            this.lbllblEmail.AutoSize = true;
            this.lbllblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblEmail.Location = new System.Drawing.Point(241, 128);
            this.lbllblEmail.Name = "lbllblEmail";
            this.lbllblEmail.Size = new System.Drawing.Size(58, 20);
            this.lbllblEmail.TabIndex = 4;
            this.lbllblEmail.Text = "Email:";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(241, 148);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(60, 24);
            this.lblEmail.TabIndex = 5;
            this.lblEmail.Text = "label4";
            // 
            // lbllblPhoneNumber
            // 
            this.lbllblPhoneNumber.AutoSize = true;
            this.lbllblPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblPhoneNumber.Location = new System.Drawing.Point(239, 180);
            this.lbllblPhoneNumber.Name = "lbllblPhoneNumber";
            this.lbllblPhoneNumber.Size = new System.Drawing.Size(132, 20);
            this.lbllblPhoneNumber.TabIndex = 6;
            this.lbllblPhoneNumber.Text = "Phone Number:";
            // 
            // lblPhoneNumber
            // 
            this.lblPhoneNumber.AutoSize = true;
            this.lblPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhoneNumber.Location = new System.Drawing.Point(238, 200);
            this.lblPhoneNumber.Name = "lblPhoneNumber";
            this.lblPhoneNumber.Size = new System.Drawing.Size(60, 24);
            this.lblPhoneNumber.TabIndex = 7;
            this.lblPhoneNumber.Text = "label6";
            // 
            // lblUsers
            // 
            this.lblUsers.AutoSize = true;
            this.lblUsers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsers.Location = new System.Drawing.Point(12, 39);
            this.lblUsers.Name = "lblUsers";
            this.lblUsers.Size = new System.Drawing.Size(136, 20);
            this.lblUsers.TabIndex = 8;
            this.lblUsers.Text = "Users in system";
            // 
            // lblHouses
            // 
            this.lblHouses.AutoSize = true;
            this.lblHouses.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouses.Location = new System.Drawing.Point(239, 235);
            this.lblHouses.Name = "lblHouses";
            this.lblHouses.Size = new System.Drawing.Size(70, 20);
            this.lblHouses.TabIndex = 9;
            this.lblHouses.Text = "Houses";
            // 
            // lbllblName
            // 
            this.lbllblName.AutoSize = true;
            this.lbllblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblName.Location = new System.Drawing.Point(483, 75);
            this.lbllblName.Name = "lbllblName";
            this.lbllblName.Size = new System.Drawing.Size(60, 20);
            this.lbllblName.TabIndex = 12;
            this.lbllblName.Text = "Name:";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(483, 95);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(60, 24);
            this.lblName.TabIndex = 13;
            this.lblName.Text = "label7";
            // 
            // lbllblServer
            // 
            this.lbllblServer.AutoSize = true;
            this.lbllblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblServer.Location = new System.Drawing.Point(483, 292);
            this.lbllblServer.Name = "lbllblServer";
            this.lbllblServer.Size = new System.Drawing.Size(66, 20);
            this.lbllblServer.TabIndex = 14;
            this.lbllblServer.Text = "Server:";
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServer.Location = new System.Drawing.Point(483, 312);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(60, 24);
            this.lblServer.TabIndex = 15;
            this.lblServer.Text = "label9";
            // 
            // lbllblDbName
            // 
            this.lbllblDbName.AutoSize = true;
            this.lbllblDbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblDbName.Location = new System.Drawing.Point(483, 348);
            this.lbllblDbName.Name = "lbllblDbName";
            this.lbllblDbName.Size = new System.Drawing.Size(143, 20);
            this.lbllblDbName.TabIndex = 16;
            this.lbllblDbName.Text = "Database Name:";
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(483, 148);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(70, 24);
            this.lblAddress.TabIndex = 17;
            this.lblAddress.Text = "label11";
            // 
            // lbllblAddress
            // 
            this.lbllblAddress.AutoSize = true;
            this.lbllblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblAddress.Location = new System.Drawing.Point(483, 128);
            this.lbllblAddress.Name = "lbllblAddress";
            this.lbllblAddress.Size = new System.Drawing.Size(80, 20);
            this.lbllblAddress.TabIndex = 18;
            this.lbllblAddress.Text = "Address:";
            // 
            // lblDbName
            // 
            this.lblDbName.AutoSize = true;
            this.lblDbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDbName.Location = new System.Drawing.Point(483, 368);
            this.lblDbName.Name = "lblDbName";
            this.lblDbName.Size = new System.Drawing.Size(70, 24);
            this.lblDbName.TabIndex = 19;
            this.lblDbName.Text = "label13";
            // 
            // lbllblState
            // 
            this.lbllblState.AutoSize = true;
            this.lbllblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblState.Location = new System.Drawing.Point(483, 235);
            this.lbllblState.Name = "lbllblState";
            this.lbllblState.Size = new System.Drawing.Size(58, 20);
            this.lbllblState.TabIndex = 20;
            this.lbllblState.Text = "State:";
            // 
            // lbllblCity
            // 
            this.lbllblCity.AutoSize = true;
            this.lbllblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblCity.Location = new System.Drawing.Point(483, 180);
            this.lbllblCity.Name = "lbllblCity";
            this.lbllblCity.Size = new System.Drawing.Size(44, 20);
            this.lbllblCity.TabIndex = 21;
            this.lbllblCity.Text = "City:";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCity.Location = new System.Drawing.Point(483, 200);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(70, 24);
            this.lblCity.TabIndex = 22;
            this.lblCity.Text = "label16";
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblState.Location = new System.Drawing.Point(483, 255);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(70, 24);
            this.lblState.TabIndex = 23;
            this.lblState.Text = "label17";
            // 
            // lbllblContact
            // 
            this.lbllblContact.AutoSize = true;
            this.lbllblContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblContact.Location = new System.Drawing.Point(483, 399);
            this.lbllblContact.Name = "lbllblContact";
            this.lbllblContact.Size = new System.Drawing.Size(77, 20);
            this.lbllblContact.TabIndex = 24;
            this.lbllblContact.Text = "Contact:";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContact.Location = new System.Drawing.Point(483, 419);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(60, 24);
            this.lblContact.TabIndex = 25;
            this.lblContact.Text = "label3";
            // 
            // lblUserInfo
            // 
            this.lblUserInfo.AutoSize = true;
            this.lblUserInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserInfo.Location = new System.Drawing.Point(242, 39);
            this.lblUserInfo.Name = "lblUserInfo";
            this.lblUserInfo.Size = new System.Drawing.Size(84, 20);
            this.lblUserInfo.TabIndex = 26;
            this.lblUserInfo.Text = "User Info";
            // 
            // lblHouseInfo
            // 
            this.lblHouseInfo.AutoSize = true;
            this.lblHouseInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouseInfo.Location = new System.Drawing.Point(483, 39);
            this.lblHouseInfo.Name = "lblHouseInfo";
            this.lblHouseInfo.Size = new System.Drawing.Size(98, 20);
            this.lblHouseInfo.TabIndex = 27;
            this.lblHouseInfo.Text = "House Info";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(734, 24);
            this.menuStrip1.TabIndex = 28;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addHouseToolStripMenuItem,
            this.editHouseToolStripMenuItem,
            this.deleteHouseToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // addHouseToolStripMenuItem
            // 
            this.addHouseToolStripMenuItem.Name = "addHouseToolStripMenuItem";
            this.addHouseToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.addHouseToolStripMenuItem.Text = "Add house";
            this.addHouseToolStripMenuItem.Click += new System.EventHandler(this.addHouseToolStripMenuItem_Click);
            // 
            // editHouseToolStripMenuItem
            // 
            this.editHouseToolStripMenuItem.Name = "editHouseToolStripMenuItem";
            this.editHouseToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.editHouseToolStripMenuItem.Text = "Edit house";
            this.editHouseToolStripMenuItem.Click += new System.EventHandler(this.editHouseToolStripMenuItem_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(154, 39);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(69, 23);
            this.btnRefresh.TabIndex = 29;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // deleteHouseToolStripMenuItem
            // 
            this.deleteHouseToolStripMenuItem.Name = "deleteHouseToolStripMenuItem";
            this.deleteHouseToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteHouseToolStripMenuItem.Text = "Delete house";
            this.deleteHouseToolStripMenuItem.Click += new System.EventHandler(this.deleteHouseToolStripMenuItem_Click);
            // 
            // IndexFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.lblHouseInfo);
            this.Controls.Add(this.lblUserInfo);
            this.Controls.Add(this.lblContact);
            this.Controls.Add(this.lbllblContact);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.lbllblCity);
            this.Controls.Add(this.lbllblState);
            this.Controls.Add(this.lblDbName);
            this.Controls.Add(this.lbllblAddress);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.lbllblDbName);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.lbllblServer);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lbllblName);
            this.Controls.Add(this.lblHouses);
            this.Controls.Add(this.lblUsers);
            this.Controls.Add(this.lblPhoneNumber);
            this.Controls.Add(this.lbllblPhoneNumber);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lbllblEmail);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lbllblUsername);
            this.Controls.Add(this.lbxHouses);
            this.Controls.Add(this.lbxUsers);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "IndexFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IndexFrm_FormClosed);
            this.Load += new System.EventHandler(this.IndexFrm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbxUsers;
        private System.Windows.Forms.ListBox lbxHouses;
        private System.Windows.Forms.Label lbllblUsername;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lbllblEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lbllblPhoneNumber;
        private System.Windows.Forms.Label lblPhoneNumber;
        private System.Windows.Forms.Label lblUsers;
        private System.Windows.Forms.Label lblHouses;
        private System.Windows.Forms.Label lbllblName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lbllblServer;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lbllblDbName;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lbllblAddress;
        private System.Windows.Forms.Label lblDbName;
        private System.Windows.Forms.Label lbllblState;
        private System.Windows.Forms.Label lbllblCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lbllblContact;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.Label lblUserInfo;
        private System.Windows.Forms.Label lblHouseInfo;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addHouseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editHouseToolStripMenuItem;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ToolStripMenuItem deleteHouseToolStripMenuItem;
    }
}