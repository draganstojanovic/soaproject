﻿using DTOLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DbServiceLibrary
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        MessageToSend AddDevices(List<DTODevice> devices);

        [OperationContract]
        MessageToSend UpdateDevices(List<DTODevice> devices);

        [OperationContract]
        MessageToSend DeleteDevices(List<DTODevice> devices);

        [OperationContract]
        List<DTODevice> GetAllDevices(string idHouse);

    }

    [DataContract]
    public class MessageToSend
    {
        [DataMember]
        public bool IsError { get; set; }
        [DataMember]
        public string Body { get; set; }
    }
}
