﻿using Amqp;
using Amqp.Listener;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using UtilsAMQP.Models;

namespace UtilsAMQP
{
    public abstract class AMQPRequestProcessor : IRequestProcessor
    {
        public static readonly HttpClient client = new HttpClient();
        public int Credit
        {
            get { return 100; }
        }

        public void Process(RequestContext requestContext)
        {
            Message msg = requestContext.Message;
            try
            {
                MessageModel obj = MessageModel.DeserializeObject((string)msg.Body);

                if (obj.IsPing)
                {
                    requestContext.Complete(new Message(MessageModel.SerializeObject(new MessageModel() { IsPing = true })));
                    return;
                }

                MessageModel retMsg = ProcessMessageInstance(obj);

                requestContext.Complete(new Message(MessageModel.SerializeObject(retMsg)));
            }
            catch (Exception e)
            {
                requestContext.Complete(new Message(MessageModel.SerializeObject(new MessageModel() { IsError = true, ErrorDescription = "Error while processing the request!\n" + e.ToString() })));
            }
        }

        public abstract MessageModel ProcessMessageInstance(MessageModel obj);
    }
}
