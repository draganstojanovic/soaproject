﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class DeviceElementsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DeviceElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DeviceElement)element).Name;
        }

        protected override string ElementName
        {
            get { return "device"; }
        }

        public DeviceElement this[int index]
        {
            get { return (DeviceElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        new public DeviceElement this[string name]
        {
            get { return (DeviceElement)BaseGet(name); }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }
    }
}

