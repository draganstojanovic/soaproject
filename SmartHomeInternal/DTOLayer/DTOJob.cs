﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DTOLayer
{
    [Serializable]
    [DataContract]
    public class DTOJob
    {
        //string deviceName;
        //string deviceType;
        [DataMember]
        string deviceTypeAndName; //deviceSignature
        [DataMember]
        string methodName;
        //[DataMember]
        [IgnoreDataMember]
        object[] methodParams;          // ignore THIS
        // 0 - JobId
        // 1 - Time
        // 2 - RepeatTimeMinutes
        // 3 - RepeatTimeSeconds
        [DataMember]
        int jobId;
        [DataMember]
        DateTime? time;
        [DataMember]
        int? repeatTimeMinutes;
        [DataMember]
        int? repeatTimeSeconds;

        [DataMember]
        public int? ActionId { get; set; }

        [DataMember]
        public bool? IsSensor { get; set; }

        [DataMember]
        public bool? IsReady { get; set; }

        [DataMember]
        public bool? IsStarted { get; set; }

        [DataMember]
        public bool? IsFinished { get; set; }

        [DataMember]
        public int? ReturnLogCount { get; set; }

        [DataMember]
        public List<DTOReturnLog> ReturnLogList
        {
            get
            {
                if (returnLogList == null)
                    returnLogList = new List<DTOReturnLog>();
                return returnLogList;
            }
            set { returnLogList = value; }
        }

        private List<DTOReturnLog> returnLogList;
        
        public string DeviceTypeAndName { get { return deviceTypeAndName; } set { deviceTypeAndName = value; } }
        public string MethodName { get { return methodName; } set { methodName = value; } }

        [DataMember]
        public int JobId { get; set; }
        public DateTime? Time { get => time; set => time = value; }
        public int? RepeatTimeMinutes { get => repeatTimeMinutes; set => repeatTimeMinutes = value; }
        public int? RepeatTimeSeconds { get => repeatTimeSeconds; set => repeatTimeSeconds = value; }
        public object[] MethodParams
        {
            get { return new object[] { jobId, time, repeatTimeMinutes, repeatTimeSeconds }; }
        }
    }
}
