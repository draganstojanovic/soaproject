﻿using DbServiceLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ServiceHostNS
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost sh = new ServiceHost(typeof(Service1)))
            {
                sh.Open();

                Console.WriteLine("Press enter to stop server.");
                Console.Read();
                sh.Close();
            }
        }
    }
}
