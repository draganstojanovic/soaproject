﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using SmartHomeAdmin.Data;
using UtilsAMQP.Models;
using UtilsAMQP;
using System.Net.Http;
using System.Configuration;

namespace SmartHomeAdmin
{
    public partial class Start : Form
    {
        public AMQPClient Client { get; }
        public ServiceReference2.Service1Client DbClient {get;}
        public States ObjectStates { get; set; }
        public Cities ObjectCities { get; set; }
        public List<User> Users { get; set; }

        public Start()
        {
            InitializeComponent();
            ObjectCities = new Cities();
            ObjectStates = new States();
            Users = new List<User>();
            DbClient = new ServiceReference2.Service1Client();
            Client = new AMQPClient();
        }

        private void Start_Load(object sender, EventArgs e)
        {
            
        }

        private void Start_Shown(object sender, EventArgs e)
        {
            //posalji zahtev za kreiranje klijenta na amqp
            bool notErr = Client.Start();

            Application.DoEvents();
            Thread.Sleep(5000);
            if (notErr)
            {
                lblMessage.Text = "Successfully connected...";
                Thread.Sleep(1000);
                //kreiraj drugu formu
                Form logIn = new LogIn();
                logIn.Show(this);
                Hide();
                return;
            }
            else
            {
                lblMessage.Text = "Server is currently unavailable...";
            }
            Thread.Sleep(1000);
            this.Dispose();
        }
    }
}
