﻿using Amqp;
using Amqp.Framing;
using System;
using UtilsAMQP.Models;

namespace UtilsAMQP
{
    public class AMQPClient : AMQPBase
    {
        private string RequestClientReceiver = "request-client-receiver";
        private string RequestClientServer = "request-client-sender";
        private Connection connection = null;
        private Session session = null;
        private SenderLink sender = null;
        private ReceiverLink receiver = null;
        private string replyTo = "";
        private string guidStr = null;
        public string GuidStr
        {
            get
            {
                if (guidStr == null)
                    guidStr = Guid.NewGuid().ToString();
                return guidStr;
            }
        }

        public string ReplyTo { get => replyTo; set => replyTo = value; }


        public AMQPClient() : base() { }
        public AMQPClient(string address, string port) : base(address, port) { }

        public bool Start()
        {
            try
            {
                replyTo = "client-" + GuidStr;

                Address address = new Address(this.Url);

                if (receiver != null)
                    receiver.Close();
                if (sender != null)
                    sender.Close();
                if (session != null)
                    session.Close();
                if (connection != null)
                    connection.Close();

                connection = new Connection(address);
                session = new Session(connection);

                Attach recvAttach = new Attach()
                {
                    Source = new Source() { Address = ServerRequestProcessorName },
                    Target = new Target() { Address = replyTo }
                };

                receiver = new ReceiverLink(session, RequestClientReceiver, recvAttach, null);
                receiver.Start(300);
                sender = new SenderLink(session, RequestClientServer, ServerRequestProcessorName);

                return true;
            }
            catch (Exception)
            {
                //Console.WriteLine("\tThe server isn't started! Start the server or contact our support. \n\tThank you.");
                return false;
            }   
        }

        /// <summary>
        /// Send and then receive a response
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="messageId">Should be unique if sending multiple messages at once. Default: "default-request"</param>
        /// <returns>object if all ok; null if something went wrong</returns>
        public MessageModel SendRecv(MessageModel obj, string messageId = "default-request")
        {
            try
            {
                Message msg = new Message(MessageModel.SerializeObject(obj));
                msg.Properties = new Properties() { MessageId = messageId, ReplyTo = replyTo };
                sender.Send(msg, null, null);

                Message msgRcv = receiver.Receive();
                return MessageModel.DeserializeObject((string)msgRcv.Body);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool Stop()
        {
            try
            {
                if (receiver != null)
                {
                    receiver.Close();
                    receiver = null;
                }
                if (sender != null)
                {
                    sender.Close();
                    sender = null;
                }
                if (session != null)
                {
                    session.Close();
                    session = null;
                }
                if (connection != null)
                {
                    connection.Close();
                    connection = null;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
