﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using DTOWeatherModels;
using SmartHomeWeather.DataLayer;

namespace SmartHomeWeather.Controllers
{
    public class WeatherController : ApiController
    {
        string URLstart = "http://api.openweathermap.org/data/2.5/weather";
        string URLend= "&APPID=71aa0198fca43cfcde8a69118e22c462";

        //GET /api/Weather
        [HttpGet]
        [Route("api/Weather/GetDataOnCurrentPlace")]
        public Object GetDataOnCurrentPlace(string mail)
        {
            Object jsonContent;

            string lat;
            string lon;

            using (var context = new SmartHomeWeatherEntities())
            {
                User user= context.Users.Where(x => x.Mail == mail).First();
                if (user == null)
                {
                    jsonContent = "Please register for this action";
                    return jsonContent;
                }
                if(!user.Active)
                {
                    jsonContent = "This client is not active";
                    return jsonContent;
                }
                lat = ((double)user.Latitude).ToString();
                lon = ((double)user.Longitude).ToString(); ;
            }
            var client = new WebClient();
            var content = client.DownloadString(URLstart + "?lat=" + lat + "&lon=" + lon+ "&units=metric"+URLend);
            var serializer = new JavaScriptSerializer();
            jsonContent = serializer.Deserialize<Object>(content);
            return jsonContent;
        }

        //GET /api/Weather
        [HttpGet]
        [Route("api/Weather/CheckSubscription")]
        public string CheeckSubscription(string mail)
        {
            string result="Success";

            using (var context = new SmartHomeWeatherEntities())
            {
                User user = context.Users.Where(x => x.Mail == mail).FirstOrDefault();
                if (user == null)
                {
                    result = "Failed";
                    return result;
                }
            }
            return result;
        }

        //GET /api/Weather
        //[HttpGet]
        //[Route("api/Weather/GetDataByCity")]
        //public Object GetDataByCity(string city)
        //{
        //    var client = new WebClient();
        //    var content = client.DownloadString(URL+"&q="+city);
        //    var serializer = new JavaScriptSerializer();
        //    var jsonContent = serializer.Deserialize<Object>(content);
        //    return jsonContent;
        //}

        //GET /api/Weather
        [HttpGet]
        [Route("api/Weather/GetDatabyCoordinate")]
        public Object GetDataByCoordinate(string lat, string lon)
        {
            var client = new WebClient();
            var content = client.DownloadString(URLstart + "?lat=" + lat+"&lon="+lon+URLend);
            var serializer = new JavaScriptSerializer();
            var jsonContent = serializer.Deserialize<Object>(content);
            return jsonContent;
        }

        //PUT /api/Weather
        [HttpPut]
        [Route("api/Weather/Activate")]
        public string Activate([FromBody] string mail)
        {
            string returnState="Success";

            using (var context = new SmartHomeWeatherEntities())
            {
                User userToActivate=context.Users.Where(x=>x.Mail==mail).First();
                if (userToActivate != null)
                {
                    userToActivate.Active = true;
                    context.SaveChanges();
                }
                else
                    return returnState = "Failed";
            }

            return returnState;
        }

        //PUT /api/Weather/Deactivate
        [HttpPut]
        [Route("api/Weather/Deactivate")]
        public Object Deactivate([FromBody] string mail)
        {
            string returnState = "Success";

            using (var context = new SmartHomeWeatherEntities())
            {
                User userToActivate = context.Users.Where(x=>x.Mail==mail).First();
                if (userToActivate != null)
                {
                    userToActivate.Active = false;
                    context.SaveChanges();
                }
                else
                    return returnState = "Failed";
            }

            return returnState;
        }

        //POST /api/Weather
        [HttpPost]
        [Route("api/Weather/SubscribeUser")]
        public string SubscribeUser([FromBody] DTOUser dtoUser)
        {
            string returnData;
            User createdUser;

            using (var context = new SmartHomeWeatherEntities())
            {
                User newUser = new User();
                newUser.Active = dtoUser.Active;
                newUser.CurrentTown = dtoUser.CurrentTown;
                newUser.Mail = dtoUser.Mail;
                newUser.RepeatTime = dtoUser.RepeatTime;
                newUser.Latitude = dtoUser.Latitude;
                newUser.Longitude = dtoUser.Longitude;
                createdUser =context.Users.Add(newUser);
                context.SaveChanges();
            }
            if (createdUser != null)
                returnData = "Success";
            else
                returnData = "Failed";

            return returnData;
        }

    }
}
