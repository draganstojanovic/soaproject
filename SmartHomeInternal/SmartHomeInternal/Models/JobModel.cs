﻿using SmartHomeLocal.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartHomeInternal.Models
{
    public class JobModel
    {
        public int Id { get; set; }
        public int ActionId { get; set; }
        public string DeviceName { get; set; }
        public string CommandCode { get; set; }
        public DateTime? Time { get; set; }
        public int? RepeatTimeMinutes { get; set; }
        public int? RepeatTimeSeconds { get; set; }
        public bool IsSensor { get; set; }
        public bool IsReady { get; set; }
        public bool IsStarted { get; set; }
        public bool IsFinished { get; set; }

        //public static List<JobModel> GetAllJobs()
        //{
        //    using (var data = new SmartHomeLocalDbEntities())
        //    {
        //        var query = from j in data.Jobs
        //                    join a in data.Actions on j.ActionId equals a.Id
        //                    join d in data.Devices on a.DeviceId equals d.Id
        //                    join c in data.Commands on a.CommandId equals c.Id
        //                    select new JobModel()
        //                    {
        //                        Id = j.Id,
        //                        ActionId = j.ActionId,
        //                        DeviceName = d.Name,
        //                        CommandCode = c.Code,
        //                        Time = j.Time,
        //                        RepeatTimeMinutes = j.RepeatTimeMinutes,
        //                        RepeatTimeSeconds = j.RepeatTimeSeconds,
        //                        IsSensor = j.IsSensor,
        //                        IsReady = j.IsReady,
        //                        IsStarted = j.IsStarted
        //                    };

        //        return query.ToList();
        //    }
        //}

        //public static JobModel AddJob(JobModel job)
        //{
        //    using (var data = new SmartHomeLocalDbEntities())
        //    {
        //        Job j = new Job()
        //        {
        //            Id = job.Id,
        //            ActionId = job.ActionId,
        //            Time = job.Time,
        //            RepeatTimeMinutes = job.RepeatTimeMinutes,
        //            RepeatTimeSeconds = job.RepeatTimeSeconds,
        //            IsSensor = job.IsSensor,
        //            IsReady = job.IsReady,
        //            IsStarted = job.IsStarted
        //        };

        //        data.Jobs.Add(j);
        //        data.SaveChanges();

        //        job.Id = j.Id;
        //        job.CommandCode = j.Action.Command.Code;
        //        job.DeviceName = j.Action.Device.Name;

        //        if (job.Time != null && (DateTime)job.Time > DateTime.Now)
        //        {
        //            DateTime jobDt = (DateTime)job.Time;

        //            string date = jobDt.ToString("yyyyMMdd");
        //            string time = jobDt.ToString("HHmmss");

        //            data.sp_add_job_quick(
        //                "job" + j.Id,
        //                "SET QUOTED_IDENTIFIER ON; update [Job] set [IsReady] = 1 where [Id] = " + j.Id,
        //                "step1_" + j.Id,
        //                "SmartHomeLocalDb",
        //                "SET QUOTED_IDENTIFIER ON; exec sp_delete_job @job_name = 'job" + j.Id + "'",
        //                "step2_" + j.Id,
        //                "msdb",
        //                "arxya-win10x64l",
        //                date,
        //                time,
        //                "jobSchedule" + j.Id);

        //            data.SaveChanges();
        //        }
        //    }

        //    return job;
        //}
    }
}