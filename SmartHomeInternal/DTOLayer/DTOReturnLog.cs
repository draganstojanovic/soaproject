﻿using System;
using System.Runtime.Serialization;

namespace DTOLayer
{
    [Serializable]
    [DataContract]
    public class DTOReturnLog
    {
        /// <summary>
        /// if IsReturnLog==false means that this is regular message that has been sent from console and ReturnValue will be string
        /// if IsReturnLog==true some method is sending response or accept confirmation
        /// </summary>
        public bool IsReturnLog { get; set; }
        /// <summary>
        /// IsSensor == true means that return log is some measurment result
        /// IsSensor == false means that this is either some action accept confirmation or console message
        /// </summary>
        [DataMember]
        public bool IsSensor { get; set; }
        /// <summary>
        /// if some unexpected situation happens IsError=true (like when user is trying to call a method on device which device does not have)
        /// or calling for action on device that is in off state
        /// or calling for on state and device is already in on state
        /// </summary>
        public bool IsError { get; set; }
        /// <summary>
        /// it is always a string, but it's value can be double if IsSensor==true
        /// </summary>
        [DataMember]
        public string ReturnValue { get; set; }

        /// <summary>
        /// Indicates if it's a string, double, int etc.
        /// </summary>
        [DataMember]
        public string ReturnType { get; set; }

        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public string Time { get; set; }

        [DataMember]
        public int? Id { get; set; }
    }
}
