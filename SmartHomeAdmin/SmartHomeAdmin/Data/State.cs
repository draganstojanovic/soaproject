﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeAdmin.Data
{
    public class State
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Abbr { get; set; }

        public override bool Equals(object obj)
        {
            var state = obj as State;
            return state != null &&
                   Id == state.Id &&
                   Name == state.Name;
        }
    }

    public class States
    {
        public List<State> AllStates { get; set; }
    }
}
