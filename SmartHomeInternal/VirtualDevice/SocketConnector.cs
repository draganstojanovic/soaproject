﻿using DTOLayer;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace VirtualDevice
{
    public class SocketConnector
    {
        private Device device;
        private Socket connector;
        private EndPoint localHome;
        private Thread receiverThread;
        private byte[] recvBuffer;
        private volatile bool conn;
        public int HomePort { get; private set; }

        public static readonly int bufferSize = 16*1024;

        public SocketConnector(Device d)
        {
            device = d;
            localHome = null;
            receiverThread = null;
            recvBuffer = new byte[bufferSize];
            conn = false;
        }

        public void Start()
        {
            HomePort = Int32.Parse(ConfigurationManager.AppSettings["HomePort"]);
            IPAddress[] home = Dns.GetHostAddresses(Dns.GetHostName());
            IPAddress IpAddress = null;
            string pattern = @"\d+\.\d+\.\d+\.\d+";
            Regex r = new Regex(pattern);
            Match m;
            foreach (var adr in home)
            {
                m = r.Match(adr.ToString());
                IPAddress a;
                if (IPAddress.TryParse(m.Value, out a))
                {
                    IpAddress = a;
                    break;
                }
            }
            if (IpAddress == null)
            {
                //dom
                IpAddress = IPAddress.Parse("10.14.90.111");
            }
            localHome = new IPEndPoint(IpAddress, HomePort);
            connector = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                connector.Connect(localHome);
                Console.WriteLine("Successfully connected to: " + localHome.ToString() + ".");
                Console.WriteLine("You are now able to send and receive messages to and from your local computer'");
                DTOReturnLog rl = new DTOReturnLog
                {
                    IsReturnLog = false,
                    IsSensor = false,
                    ReturnValue = device.DeviceType + ":" + device.DeviceName
                };
                SendMessage(rl); //predstavljenje uredjaja
                StartReceiving();
                conn = true;
                while (conn)
                {
                    try
                    {
                        string msg = Console.ReadLine();
                        if (msg.Equals("help", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Console.WriteLine("Possible commands:");
                            Console.WriteLine("configuration");
                            Console.WriteLine("exit");
                        }
                        if (msg.Equals("configuration", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Console.WriteLine("List of actions for {0} - {1}:", device.DeviceType, device.DeviceName);
                            int i = 1;
                            foreach (string act in DeviceState.Actions)
                            {
                                Console.WriteLine(i++ + "." + act);
                            }
                        }
                        if (msg.Equals("exit", StringComparison.CurrentCultureIgnoreCase))
                        {
                            rl.ReturnValue = msg;
                            SendMessage(rl);
                            conn = false;
                        }
                        else
                        {
                            Console.WriteLine("This is not a valid command. To see the list of valid commands type help.");
                        }
                    }
                    catch (Exception e)
                    {
                        //Conn = false;
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("SocketConnector.cs/Start");
                        Console.WriteLine("Message:" + e.Message);
                        Console.WriteLine("Inner Exception:" + e.InnerException);
                        Console.WriteLine("Source" + e.Source);
                        Console.WriteLine("StackTrace" + e.StackTrace);
                        Console.ResetColor();
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }

        private void StartReceiving()
        {
            receiverThread = new Thread(new ThreadStart(ReceiveMessages));
            receiverThread.Start();
        }

        public void ReceiveMessages()
        {
            connector.ReceiveTimeout = -1;
            connector.ReceiveBufferSize = bufferSize;
            while (conn)
            {
                try
                {
                    int read = connector.ReceiveFrom(recvBuffer, ref localHome);
                    if (read > 0)
                    {
                        //string s = Encoding.UTF8.GetString(recvBuffer, 0, read);
                        DTOJob act = DeserializeObject(recvBuffer);
                        DTOReturnLog rl = device.Command(act);
                        SendMessage(rl);
                    }
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("SocketConnector.cs/ReceiveMessages");
                    Console.WriteLine("Message:" + e.Message);
                    Console.WriteLine("Inner Exception:" + e.InnerException);
                    Console.WriteLine("Source" + e.Source);
                    Console.WriteLine("StackTrace" + e.StackTrace);
                    Console.ResetColor();
                    Disconnect();
                    conn = false;
                }
            }
        }

        public void Stop()
        {
            if (conn)
                return;
            try
            {
                if (connector.Connected)
                {
                    connector.Shutdown(SocketShutdown.Both);
                    connector.Close();
                }
                if (receiverThread != null)
                {
                    receiverThread.Abort();
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("SocketConnector.cs/Stop");
                Console.WriteLine("Message:" + e.Message);
                Console.WriteLine("Inner Exception:" + e.InnerException);
                Console.WriteLine("Source" + e.Source);
                Console.WriteLine("StackTrace" + e.StackTrace);
                Console.ResetColor();
                if (receiverThread != null)
                    receiverThread.Join();
            }
        }

        private byte[] SerializeObject(DTOReturnLog rl)
        {
            using (MemoryStream ms = new MemoryStream(bufferSize))
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, rl);
                byte[] buffer = new byte[bufferSize];
                ms.Seek(0, SeekOrigin.Begin);
                if (ms.Length < bufferSize+1)
                    ms.Read(buffer, 0, (int)ms.Length);
                return buffer;
            }
        }

        private DTOJob DeserializeObject(byte[] buffer)
        {
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                IFormatter formatter = new BinaryFormatter();
                DTOJob act;
                try
                {
                    act = (DTOJob)formatter.Deserialize(ms);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("SocketConnector.cs/DeserializeObject");
                    Console.WriteLine("Message:" + e.Message);
                    Console.WriteLine("Inner Exception:" + e.InnerException);
                    Console.WriteLine("Source" + e.Source);
                    Console.WriteLine("StackTrace" + e.StackTrace);
                    Console.ResetColor();
                    act = null;
                }
                return act;
            }
        }

        public void SendMessage(string msg)
        {
            try
            {
                byte[] buffer = Encoding.UTF8.GetBytes(msg);
                int sendBytes = -1;
                Connect();
                sendBytes = connector.Send(buffer);
                return;
            }
            catch (ArgumentNullException ane)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ane.StackTrace);
                Disconnect();
                throw;
            }
            catch (SocketException se)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(se.StackTrace);
                Disconnect();
                throw;
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.StackTrace);
                Disconnect();
                throw;
            }
        }

        public void SendMessage(DTOReturnLog rl)
        {
            if (rl == null)
                throw new ArgumentNullException("rl", "DTOReturnLog was null in SocketConnector.cs/SendMessage");
            try
            {
                byte[] buffer = SerializeObject(rl);
                int sendBytes = -1;
                Connect();
                sendBytes = connector.Send(buffer);
                return;
            }
            catch (ArgumentNullException ane)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ane.StackTrace);
                Disconnect();
                throw;
            }
            catch (SocketException se)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(se.StackTrace);
                Disconnect();
                throw;
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(e.StackTrace);
                Disconnect();
                throw;
            }
        }

        private void Disconnect()
        {
            if (connector.Connected)
            {
                connector.Shutdown(SocketShutdown.Both);
                connector.Disconnect(true);
            }
        }

        private void Connect()
        {
            if (connector.Connected)
                return;
            connector.Connect(localHome);
        }
    }
}
