﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOWeatherModels
{
    public class DTOUser
    {
        public string Mail { get; set; }
        public bool Active { get; set; }
        public int RepeatTime { get; set; }
        public string CurrentTown { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}
