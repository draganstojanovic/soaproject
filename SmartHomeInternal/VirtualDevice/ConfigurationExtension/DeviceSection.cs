﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class DevicesSection : ConfigurationSection
    {
        private static ConfigurationPropertyCollection _propertyCollection;
        private static readonly ConfigurationProperty _deviceTypes;
        //private static readonly ConfigurationProperty _xmlnsNamespace;

        public DeviceTypeElementsCollection DeviceTypes
        {
            get { return (DeviceTypeElementsCollection)this[_deviceTypes]; }
        }

        //public string XmlnsNamespace
        //{
        //    get { return (string)this[_xmlnsNamespace]; }
        //}

        protected override ConfigurationPropertyCollection Properties
        {
            get { return _propertyCollection; }
        }

        static DevicesSection()
        {
            //_xmlnsNamespace = new ConfigurationProperty("xmlns", typeof(string), null, ConfigurationPropertyOptions.IsRequired);
            _deviceTypes = new ConfigurationProperty("deviceTypes", typeof(DeviceTypeElementsCollection), null, ConfigurationPropertyOptions.None);

            _propertyCollection = new ConfigurationPropertyCollection();
            _propertyCollection.Add(_deviceTypes);
        }
    }
}
