﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using SmartHomeMain.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace SmartHomeMVC.Helpers
{
    public interface ICleanRabbitMqConsumer
    {

    }

    public class CleanRabbitMqConsumer : ICleanRabbitMqConsumer
    {
        private string _address;
        private string _queue;
        public CleanRabbitMqConsumer(string address)
        {
            _address = address;
        }

        public CleanRabbitMqConsumer()
        {
            _address = "localhost";
            Consume("queue1");
        }

        public void Consume(string queue)
        {
            _queue = queue;

            var factory = new ConnectionFactory() { HostName = _address };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _queue,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    using (var data = new SmartHomeMainDbEntities())
                    {
                        House h = data.Houses.Where(x => x.Id == 2).FirstOrDefault();
                        data.Houses.Remove(h);
                        data.SaveChanges();
                    }
                };

                channel.BasicConsume(queue: _queue,
                                     autoAck: true,
                                     consumer: consumer);
            }
        }
    }
}