﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text.RegularExpressions;
using System.Threading;
using DTOLayer;
using Microsoft.AspNet.SignalR;
using SmartHomeInternal.SignalRHubs;
using SmartHomeLocal.DataLayer;

namespace SmartHomeInternal
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple,
                    InstanceContextMode = InstanceContextMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.NotAllowed)]
    public class Service1 : IService1
    {
        public Socket Listener { get; set; }
        public IPAddress IpAddress { get; set; }
        public bool Receiving { get; set; }
        public static Dictionary<string, Socket> Devices { get; set; }

        public static readonly int bufferSize =  16*1024;

        private Thread t;

        public Service1()
        {
            Devices = new Dictionary<string, Socket>();
            IPAddress[] addrs = Dns.GetHostAddresses(Dns.GetHostName());
            //IpAddress = addrs.Where(x => x.MapToIPv4().ToString().Contains("192.168.")).Select(x => x).First();
            string pattern = @"\d+\.\d+\.\d+\.\d+";
            Regex r = new Regex(pattern);
            Match m;
            foreach (var adr in addrs)
            {
                m = r.Match(adr.ToString());
                IPAddress a;
                if (IPAddress.TryParse(m.Value, out a))
                {
                    IpAddress = a;
                    break;
                }
            }
            if (IpAddress == null)
            {
                //dom
                IpAddress = IPAddress.Parse("10.14.90.111");
            }
            t = null;
            Listener = null;
        }

        ~Service1()
        {
            Receiving = false;
            if (t != null)
                t.Abort();
            Listener.Close();
        }

        public MessageToSend SendMessage(DTOJob ate)
        {
            if(ate.Time != null)
            {
                // if retrospective, set to null so it could be done immediately
                if (ate.Time <= DateTime.UtcNow.AddSeconds(30))
                    ate.Time = null;
                // else change it to local time, since the bootstrap datetimepicker returns UTC by default
                else
                    ate.Time = ((DateTime)ate.Time).ToLocalTime();
            }

            try
            {
                int sendBytes = -1;
                if (ate.DeviceTypeAndName == null)
                    throw new ArgumentNullException("Device signature does not exist.");
                if (Devices.ContainsKey(ate.DeviceTypeAndName))
                {
                    // pomereno u JobDependent
                    //byte[] buffer = SerializeObject(ate);
                    //sendBytes = devices[ate.DeviceTypeAndName].Send(buffer);

                    string[] typeName = ate.DeviceTypeAndName.Split(':');
                    string type = typeName[0];
                    string name = typeName[1];
                    using (var data = new SmartHomeLocalDbEntities())
                    {
                        var device = data.Device.Where(x => x.Type.Equals(type) && x.Name.Equals(name)).FirstOrDefault();
                        if (device == null)
                            throw new Exception(string.Format("Device {0} {1} not found!!!", typeName[0], typeName[1]));

                        var action = data.Action.Where(x => x.DeviceId == device.Id && x.Name.Equals(ate.MethodName)).FirstOrDefault();
                        if (action == null)
                            throw new Exception(string.Format("Action {0} not found!!!", ate.MethodName));

                        Job j = new Job();
                        j.ActionId = action.Id;
                        j.Time = (DateTime?)ate.Time;
                        j.RepeatTimeMinutes = (int?)ate.RepeatTimeMinutes;
                        j.RepeatTimeSeconds = (int?)ate.RepeatTimeSeconds;                        

                        data.Job.Add(j);

                        data.SaveChanges();

                        ate.JobId = j.Id;

                        if (j.Time != null && (DateTime)j.Time > DateTime.Now)
                        {
                            DateTime jobDt = (DateTime)j.Time;

                            string date = jobDt.ToString("yyyyMMdd");
                            string time = jobDt.ToString("HHmmss");

                            data.sp_add_job_quick(
                                "job" + j.Id,
                                "SET QUOTED_IDENTIFIER ON; update [Job] set [IsReady] = 1 where [Id] = " + j.Id,
                                "step1_" + j.Id,
                                "SmartHomeLocalDb",
                                "SET QUOTED_IDENTIFIER ON; exec sp_delete_job @job_name = 'job" + j.Id + "'",
                                "step2_" + j.Id,
                                "msdb",
                                data.Database.Connection.DataSource,
                                date,
                                time,
                                "jobSchedule" + j.Id);

                            data.SaveChanges();
                        }
                        else
                        {
                            byte[] buffer = SerializeObject(ate);
                            sendBytes = Devices[ate.DeviceTypeAndName].Send(buffer);
                        }
                    }
                }
                else
                    throw new Exception("There is no connection to device: " + ate.DeviceTypeAndName);
                return new MessageToSend() { IsError = false, Body = sendBytes.ToString() };
            }
            catch (ArgumentNullException ane)
            {
                return new MessageToSend() { IsError = true, Body = "ArgumentNullException : " + ane.ToString() };
            }
            catch (SocketException se)
            {
                return new MessageToSend() { IsError = true, Body = "SocketException : " + se.ToString() };
            }
            catch (Exception e)
            {
                return new MessageToSend() { IsError = true, Body = "Unexpected exception : " + e.ToString() };
            }
        }

        public static byte[] SerializeObject(DTOJob ate)
        {
            using (MemoryStream ms = new MemoryStream(bufferSize))
            {
                IFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, ate);
                byte[] buffer = new byte[bufferSize];
                ms.Seek(0, SeekOrigin.Begin);
                if (ms.Length < bufferSize+1)
                    ms.Read(buffer, 0, bufferSize);
                return buffer;
            }
        }

        private DTOReturnLog DeserializeObject(byte[] buffer)
        {
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                IFormatter formatter = new BinaryFormatter();
                DTOReturnLog rl;
                try
                {
                    rl = (DTOReturnLog)formatter.Deserialize(ms);
                }
                catch (Exception e)
                {
                    //Console.ForegroundColor = ConsoleColor.Yellow;
                    //Console.WriteLine("SocketConnector.cs/DeserializeObject");
                    //Console.WriteLine("Message:" + e.Message);
                    //Console.WriteLine("Inner Exception:" + e.InnerException);
                    //Console.WriteLine("Source" + e.Source);
                    //Console.WriteLine("StackTrace" + e.StackTrace);
                    //Console.ResetColor();
                    rl = null;
                }
                return rl;
            }
        }

        private void SignalRMessage(DTOReturnLog rl)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<UniversalHub>();
            context.Clients.All.returnLog(rl);
        }

        public void StartListening()
        {
            if (Listener != null)
                return;
            IPAddress[] addrs = Dns.GetHostAddresses(Dns.GetHostName());
            IPEndPoint localEndPoint = new IPEndPoint(IpAddress, 12000);
            Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                Listener.Bind(localEndPoint);
                Listener.Listen(10);
                //Console.WriteLine("Listening at: " + localEndPoint.ToString() + ".");
                t = new Thread(new ThreadStart(Listening))
                {
                    IsBackground = true
                };
                t.Start();
            }
            catch (Exception e)
            {
                if (t != null)
                    t.Abort();
            }
        }

        private void Listening()
        {
            bool Receiving = true;
            try
            {
                while (Receiving)
                {
                    IAsyncResult a = Listener.BeginAccept(new AsyncCallback(AcceptCallback), Listener);
                    a.AsyncWaitHandle.WaitOne();
                }
                Listener.Close();
            }
            catch (Exception) { }
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);
            handler.ReceiveTimeout = -1;
            handler.ReceiveBufferSize = bufferSize;
            byte[] buffer = new byte[bufferSize];
            int read = handler.Receive(buffer);
            DTOReturnLog rl = DeserializeObject(buffer);
            string DeviceTypeAndName = rl.ReturnValue; // returnValue in first message represents device name & type
            if (!Devices.ContainsKey(rl.ReturnValue))
                Devices.Add(DeviceTypeAndName, handler);
            else
                throw new Exception("There is already a connection between those two devices!");
            while (Devices.ContainsKey(DeviceTypeAndName) && Devices[DeviceTypeAndName] != null)
            {
                try
                {
                    read = handler.Receive(buffer);
                    if (!(read > 0)) continue;
                    rl = DeserializeObject(buffer);
                    if (!rl.IsError && !rl.IsReturnLog && !rl.IsSensor)
                    {
                        if (rl.ReturnValue.Equals("exit", StringComparison.CurrentCultureIgnoreCase))
                        {
                            Devices.Remove(DeviceTypeAndName);
                            //handler.Send(Encoding.ASCII.GetBytes("confirmed"));
                        }
                    }
                    else
                    {
                        if (!rl.IsError)
                        {
                            using (var data = new SmartHomeLocalDbEntities())
                            {
                                ReturnLog nrl = new ReturnLog()
                                {
                                    JobId = rl.JobId,
                                    Time = DateTime.UtcNow,
                                    Value = rl.ReturnValue,
                                    ValueType = rl.ReturnType
                                };
                                data.ReturnLog.Add(nrl);
                                data.SaveChanges();
                            }
                        }
                        SignalRMessage(rl);
                    }
                }
                catch (Exception) { }
            }
            handler.Shutdown(SocketShutdown.Receive);
            handler.Close();
        }

        public void CreateInstance()
        {
            StartListening();
        }

        public List<DTODevice> GetDevices()
        {
            try
            {
                using (var data = new SmartHomeLocalDbEntities())
                {
                    var query = data.Device.Select(x => new DTODevice()
                    {
                        DateOfReg = x.DateOfReg,
                        DateOfRegStr = x.DateOfReg.ToString(),
                        Id = x.Id,
                        Name = x.Name,
                        URL = x.URL,
                        Type = x.Type,
                        ActionList = data.Action.Where(a => a.DeviceId == x.Id).Select(a => new DTOAction()
                        {
                            Id = a.Id,
                            DeviceId = a.DeviceId,
                            Description = a.Description,
                            IsRepeatable = a.IsRepeatable ?? false,
                            MeasureUnit = a.MeasureUnit,
                            Name = a.Name
                        }).ToList()
                    });

                    var toRet = query.ToList();
                    return toRet;
                }
            }
            catch (Exception) { return new List<DTODevice>(); }
        }

        public DTODevice GetDeviceInfo(int id)
        {
            try
            {
                using (var data = new SmartHomeLocalDbEntities())
                {
                    var query = data.Device.Where(x => x.Id == id).Select(x => new DTODevice()
                    {
                        DateOfReg = x.DateOfReg,
                        DateOfRegStr = x.DateOfReg.ToString(),
                        Id = x.Id,
                        Name = x.Name,
                        URL = x.URL,
                        Type = x.Type,
                        ActionList = data.Action.Where(a => a.DeviceId == x.Id).Select(a => new DTOAction()
                        {
                            Id = a.Id,
                            DeviceId = a.DeviceId,
                            Description = a.Description,
                            IsRepeatable = a.IsRepeatable ?? false,
                            MeasureUnit = a.MeasureUnit,
                            Name = a.Name,
                            JobList = data.Job.Where(j => j.ActionId == a.Id).Select(j => new DTOJob()
                            {
                                DeviceTypeAndName = j.Action.Device.Type + ":" + j.Action.Device.Name,
                                JobId = j.Id,
                                MethodName = j.Action.Name,
                                RepeatTimeMinutes = j.RepeatTimeMinutes,
                                RepeatTimeSeconds = j.RepeatTimeSeconds,
                                Time = j.Time,
                                ActionId = j.ActionId,
                                IsReady = j.IsReady,
                                IsFinished = j.IsFinished,
                                IsStarted = j.IsStarted,
                                IsSensor = j.IsSensor,
                                ReturnLogCount = data.ReturnLog.Where(rl => rl.JobId == j.Id).Count()
                            }).ToList()
                        }).ToList()
                    });

                    var toRet = query.ToList().FirstOrDefault();

                    foreach (var act in toRet.ActionList)
                        foreach (var job in act.JobList)
                        {
                            job.ReturnLogCount = data.ReturnLog.Where(rl => rl.JobId == job.JobId).Select(rl => new DTOReturnLog()
                            {
                                Id = rl.Id,
                                JobId = id,
                                Time = rl.Time.ToString(),
                                ReturnValue = rl.Value,
                                ReturnType = rl.ValueType,
                                IsSensor = rl.IsSensor ?? false
                            }).ToList().Count();
                        }
                    return toRet;
                }
            }
            catch (Exception) { return null; }
        }

        public DTOJob GetJobInfo(int id)
        {
            try
            {
                using (var data = new SmartHomeLocalDbEntities())
                {
                    var query = data.Job.Where(j => j.Id == id).Select(j => new DTOJob()
                    {
                        DeviceTypeAndName = j.Action.Device.Type + ":" + j.Action.Device.Name,
                        JobId = j.Id,
                        MethodName = j.Action.Name,
                        RepeatTimeMinutes = j.RepeatTimeMinutes,
                        RepeatTimeSeconds = j.RepeatTimeSeconds,
                        Time = j.Time,
                        ActionId = j.ActionId,
                        IsReady = j.IsReady,
                        IsFinished = j.IsFinished,
                        IsStarted = j.IsStarted,
                        IsSensor = j.IsSensor,
                        ReturnLogList = data.ReturnLog.Where(x => x.JobId == id).Select(rl => new DTOReturnLog()
                        {
                            Id = rl.Id,
                            JobId = id,
                            Time = rl.Time.ToString(),
                            ReturnValue = rl.Value,
                            ReturnType = rl.ValueType,
                            IsSensor = rl.IsSensor ?? false                             
                        }).ToList(),
                        ReturnLogCount = data.ReturnLog.Where(rl => rl.JobId == j.Id).Count()
                    });

                    var toRet = query.ToList().FirstOrDefault();
                    return toRet;
                }
            }
            catch (Exception) { return null; }
        }
    }
}
