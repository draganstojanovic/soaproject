﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeAdmin.Data
{
    public class Action
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsRepeatable { get; set; }
        public string MeasurementUnit { get; set; }
    }
}
