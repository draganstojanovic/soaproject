﻿using SmartHomeAdmin.Data;
using SmartHomeAdmin.ServiceReference2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsAMQP.Models;

namespace SmartHomeAdmin
{
    public partial class IndexFrm : Form
    {
        public IndexFrm()
        {
            InitializeComponent();
        }

        private void IndexFrm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Dispose();
        }

        private void IndexFrm_Load(object sender, EventArgs e)
        {
            //u house treba userId?

            lbxUsers.DisplayMember = "UserName";
            lbxUsers.ValueMember = "Id";
            lbxUsers.DataSource = ((Start)Owner).Users;
        }

        private void lbxUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxUsers.SelectedIndex == -1)
                return;
            User u = lbxUsers.SelectedItem as User;
            if (u == null)
                MessageBox.Show("Some internal problem persists.");
            lblUsername.Text = u.UserName;
            lblEmail.Text = u.Email;
            lblPhoneNumber.Text = u.PhoneNumber;

            List<House> hh1 = new List<House>();
            List<House> hh2 = new List<House>();
            if (!u.Loaded)
            {
                //posalji zahtev za kucama korisnika
                MessageModel houses = ((Start)Owner).Client.SendRecv(new MessageModel()
                { MethodName = "GetAllHousesForUser",
                    RequestResponse = new List<object>() { new DTOUser() { Id = u.Id } } });
                if(houses.IsError)
                {
                    MessageBox.Show(houses.ErrorDescription);
                    return;
                }

                if (u.Houses == null)
                    u.Houses = new List<House>();

                foreach(object h in houses.RequestResponse)
                {
                    DTOHouse ho = h as DTOHouse;
                    Data.House newH = new House()
                    {
                        Address = ho.Address,
                        CityId = ho.CityId,
                        CityName = ho.CityName,
                        Contact = ho.Contact,
                        DbName = ho.DbName,
                        Devices = new List<Device>(),
                        Id = ho.Id,
                        Ip = ho.Ip,
                        Name = ho.Name,
                        Port = ho.Port,
                        StateAbbr = ho.StateAbbr,
                        StateId = ho.StateId,
                        StateName = ho.StateName,
                        UserId = ho.UserId,
                        UserName = ho.UserName
                    };

                    //pribavi devices za svaku kucu!
                    List<Device> devs = new List<Device>();
                    List<DTODevice> devices = ((Start)Owner).DbClient.GetAllDevices(ho.Id).ToList();
                    foreach(var dev in devices)
                    {
                        List<Data.Action> actions = new List<Data.Action>();
                        foreach(var a in dev.ActionList)
                        {
                            Data.Action action = new Data.Action()
                            {
                                Description = a.Description,
                                Id = "0",
                                IsRepeatable = a.IsRepeatable,
                                MeasurementUnit = a.MeasureUnit,
                                Name = a.Name
                            };
                        }
                        Device d = new Device()
                        {
                            DateOfReg = dev.DateOfReg.ToString(),
                            Id = dev.Id.ToString(),
                            Name = dev.Name,
                            Type = dev.Type,
                            Actions = actions
                        };
                        newH.Devices.Add(d);
                    }
                    
                    u.Houses.Add(newH);
                }

                u.Loaded = true;
            }
            if(lbxHouses.DataSource==null)
            {
                lbxHouses.DisplayMember = "Name";
                lbxHouses.ValueMember = "Id";
            }
            else
                lbxHouses.DataSource = null;
            lbxHouses.DataSource = u.Houses;
            lbxHouses.DisplayMember = "Name";
            lbxHouses.ValueMember = "Id";

        }

        private void lbxHouses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxHouses.SelectedIndex == -1)
                return;
            House h = lbxHouses.SelectedItem as House;
            if (h == null)
                MessageBox.Show("Some internal problem persists.");
            lblAddress.Text = h.Address;
            lblCity.Text = h.CityName;
            lblContact.Text = h.Contact;
            lblDbName.Text = h.DbName;
            lblServer.Text = h.Ip+":"+h.Port;
            lblName.Text = h.Name;
            lblState.Text = h.StateName;
        }

        private void addHouseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            User u = ((Start)Owner).Users.Where(x => x.Id.Equals(lbxUsers.SelectedValue)).FirstOrDefault();
            if (u != null)
            {
                Form f = new AddHouse(u);
                f.Owner = this.Owner;
                f.Show();
                this.Dispose();
            }
            else
            {
                MessageBox.Show("User is not selected!");
            }
        }

        private void editHouseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            User u = ((Start)Owner).Users.Where(x => x.Equals(lbxUsers.SelectedItem as User)).FirstOrDefault();
            if (u != null)
            {
                House h = u.Houses.Where(x => x.Equals(lbxHouses.SelectedItem as House)).FirstOrDefault();
                if (h != null)
                {
                    Form f = new EditHouse(h, lbxHouses.SelectedIndex);
                    f.Owner = this.Owner;
                    f.Show();
                    this.Dispose();
                }
                else
                    MessageBox.Show("House is not selected!");
            }
            else
            {
                MessageBox.Show("User is not selected!");
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            //ucitaj ponovo sve i korisnike i drzave i gradove
            MessageModel cities = ((Start)Owner).Client.SendRecv(new MessageModel()
            {
                MethodName = "GetAllCities"
            });

            if (cities.IsError)
            {
                MessageBox.Show(cities.ErrorDescription);
                return;
            }

            MessageModel states = ((Start)Owner).Client.SendRecv(new MessageModel()
            {
                MethodName = "GetAllStates"
            });

            if (states.IsError)
            {
                MessageBox.Show(states.ErrorDescription);
                return;
            }
            MessageModel users = ((Start)Owner).Client.SendRecv(new MessageModel()
            {
                MethodName = "GetAllUsers"
            });

            if (users.IsError)
            {
                MessageBox.Show(users.ErrorDescription);
                return;
            }

                ((Start)Owner).Users = new List<Data.User>();
            foreach (object u in users.RequestResponse)
            {
                DTOUser user = u as DTOUser;
                Data.User newUs = new Data.User()
                {
                    Email = user.Email,
                    Houses = null,
                    Id = user.Id,
                    Loaded = false,
                    Password = user.Password,
                    PhoneNumber = user.PhoneNumber,
                    UserName = user.UserName
                };
                ((Start)Owner).Users.Add(newUs);
            }

                ((Start)Owner).ObjectCities.AllCities = new List<Data.City>();
            foreach (object u in cities.RequestResponse)
            {
                DTOCity city = u as DTOCity;
                Data.City newUs = new Data.City()
                {
                    Id = city.Id,
                    Name = city.Name,
                    StateAbbr = city.StateAbbr,
                    StateName = city.StateName
                };
                ((Start)Owner).ObjectCities.AllCities.Add(newUs);
            }

                ((Start)Owner).ObjectStates.AllStates = new List<Data.State>();
            foreach (object u in states.RequestResponse)
            {
                DTOState state = u as DTOState;
                Data.State newUs = new Data.State()
                {
                    Id = state.Id,
                    Name = state.Name,
                    Abbr = state.Abbr
                };
                ((Start)Owner).ObjectStates.AllStates.Add(newUs);
            }

            lbxUsers.DataSource = null;
            lbxUsers.DataSource = ((Start)Owner).Users;
            lbxUsers.DisplayMember = "UserName";
            lbxUsers.ValueMember = "Id";
        }

        private void deleteHouseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (lbxHouses.SelectedIndex == -1)
                return;
            User u = ((Start)Owner).Users.Where(x => x.Equals(lbxUsers.SelectedItem as User)).FirstOrDefault();
            if (u != null)
            {
                House h = u.Houses.Where(x => x.Equals(lbxHouses.SelectedItem as House)).FirstOrDefault();
                if (h != null)
                {
                    if(MessageBox.Show("Are you sure you want to delete this House?\nChanges cannot be reverted!", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    {
                        return;
                    }

                    //obrisi house u glavnoj bazi i iz lokalne za tu kucu kompletnu bazu!!!
                    // -- // -- // -- //
                    MessageModel mts = ((Start)Owner).Client.SendRecv(new MessageModel()
                    {
                        MethodName = "DeleteHouse",
                        RequestResponse = new List<object>()
                        {
                            new DTOHouse(){Id = h.Id}
                        }
                    });
                    if (mts.IsError)
                        MessageBox.Show("There has been an error while saving house in database");
                    List<DTODevice> Devs = new List<DTODevice>();
                    foreach (var d in h.Devices)
                    {
                        var actions = new List<DTOAction>();
                        foreach (var a in d.Actions)
                        {
                            actions.Add(new DTOAction()
                            {
                                Description = a.Description,
                                DeviceId = 0,
                                IsRepeatable = a.IsRepeatable,
                                MeasureUnit = a.MeasurementUnit,
                                Name = a.Name,
                                Id = string.IsNullOrEmpty(a.Id) ? 0 : int.Parse(a.Id)
                            });
                        }
                        Devs.Add(new DTODevice()
                        {
                            ActionList = actions.ToArray(),
                            DateOfReg = DateTime.UtcNow,
                            Id = 0,
                            Name = d.Name,
                            Type = d.Type,
                            URL = ""
                        });
                    }
                    MessageToSend m1 = ((Start)Owner).DbClient.DeleteDevices(Devs.ToArray());
                    if (m1 == null)
                    {
                        MessageBox.Show("Upss. Something went wrong.");
                        return;
                    }
                    //MessageBox.Show(m1.Body);
                    if (mts.IsError)
                    {
                        MessageBox.Show(m1.Body);
                        return;
                    }
                    MessageBox.Show("House has been successfully deleted.");

                    // ovde ne radi lepo refresh u lbxHouses
                    u.Houses.RemoveAt(lbxHouses.SelectedIndex);
                    if (u.Houses != null)
                    {
                        lbxHouses.DataSource = null;
                        lbxHouses.DataSource = u.Houses;
                        lbxHouses.DisplayMember = "Name";
                        lbxHouses.ValueMember = "Id";
                    }
                    //lbxHouses.Refresh();
                }
                else
                    MessageBox.Show("House is not selected!");
            }
            else
            {
                MessageBox.Show("User is not selected!");
            }
        }
    }
}
