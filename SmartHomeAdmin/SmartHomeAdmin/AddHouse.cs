﻿using SmartHomeAdmin.Data;
using SmartHomeAdmin.ServiceReference2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsAMQP.Models;

namespace SmartHomeAdmin
{
    public partial class AddHouse : Form
    {
        public User HouseOwner { get; set; }
        //int selIndex = -1;
        public AddHouse()
        {
            InitializeComponent();
        }

        public AddHouse(User u): this()
        {
            HouseOwner = u;
        }

        private void AddHouse_Load(object sender, EventArgs e)
        {
            cbxState.ValueMember = "Id";
            cbxState.DisplayMember = "Name";
            cbxState.DataSource = ((Start)Owner).ObjectStates.AllStates;

            cbxCity.ValueMember = "Id";
            cbxCity.DisplayMember = "Name";
            cbxCity.DataSource = ((Start)Owner).ObjectCities.AllCities;
            lblHouseOwner.Text = HouseOwner.UserName;
            Application.DoEvents();
        }

        private void AddHouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Dispose();
        }

        private void AddHouse_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to quit?", "Quiting application"
                    , MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
            List<Device> devices = new List<Device>();
            foreach (var d in lbxDevices.Items)
            {
                devices.Add(d as Device);
            }
            City c = cbxCity.SelectedItem as City;
            State st = cbxState.SelectedItem as State;
            House h = new House()
            {
                 UserName = HouseOwner.UserName,
                 UserId = HouseOwner.Id,
                 Name = tbxName.Text,
                 Ip = s[0],
                 Port= s[1],
                 Address = tbxAddress.Text,
                CityName = c != null ? c.Name : "",
                CityId = c != null ? c.Id : "",
                StateId = st != null ? st.Id : "",
                StateName = st != null ? st.Name : "",
                Contact = tbxContact.Text,
                 DbName = tbxDbName.Text,
                 Devices = devices
            };
            Form f = new AddDevice(h);
            f.Owner = this.Owner;
            f.Show();
            this.Owner = null;
            this.Dispose();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
            List<Device> devices = new List<Device>();
            foreach (var d in lbxDevices.Items)
            {
                devices.Add(d as Device);
            }
            City c = cbxCity.SelectedItem as City;
            State st = cbxState.SelectedItem as State;
            House h = new House()
            {
                UserName = HouseOwner.UserName,
                UserId = HouseOwner.Id,
                Name = tbxName.Text,
                Ip = s[0],
                Port = s[1],
                Address = tbxAddress.Text,
                CityName = c != null ? c.Name : "",
                CityId = c != null ? c.Id : "",
                StateId = st != null ? st.Id : "",
                StateName = st != null ? st.Name : "",
                Contact = tbxContact.Text,
                DbName = tbxDbName.Text,
                Devices = devices
            };
            Device dev = lbxDevices.SelectedItem as Device;
            Form f = new EditDevice(h, dev, lbxDevices.SelectedIndex);
            f.Owner = this.Owner;
            f.Show();
            this.Owner = null;
            this.Dispose();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lbxDevices.SelectedIndex == -1)
                return;
            Device d = lbxDevices.SelectedItem as Device;
            if (d == null)
                MessageBox.Show("Device does not exist!");
            lbxDevices.Items.Remove(d);
            lbxDevices.Refresh();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to submit?"
                    , "Submitting house"
                    , MessageBoxButtons.OKCancel
                    , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                //posalji house glavnoj bazi, pa onda house devices lokalnoj
                User u = ((Start)Owner).Users.Where(x => x.Id.Equals(HouseOwner.Id)).FirstOrDefault();
                if (u == null)
                    return;
                string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
                City c = cbxCity.SelectedItem as City;
                State st = cbxState.SelectedItem as State;
                House h = new House()
                {
                    UserName = HouseOwner.UserName,
                    UserId = HouseOwner.Id,
                    Name = tbxName.Text,
                    Ip = s[0],
                    Port = s[1],
                    Address = tbxAddress.Text,
                    CityName = c != null ? c.Name : "",
                    CityId = c != null ? c.Id : "",
                    StateId = st != null ? st.Id : "",
                    StateName = st != null ? st.Name : "",
                    Contact = tbxContact.Text,
                    DbName = tbxDbName.Text,
                    Devices = null
                };

                MessageModel received = ((Start)Owner).Client.SendRecv(new MessageModel()
                {
                    MethodName = "AddHouse",
                    RequestResponse = new List<object>() { new DTOHouse()
                        {
                             Ip = h.Ip,
                              Name = h.Name,
                               Id = "",
                                Address = h.Address,
                                 CityId = h.CityId,
                                  CityName = h.CityName,
                                   Contact = h.Contact,
                                    DbName = h.DbName,
                                     Port = h.Port,
                                      StateAbbr = h.StateAbbr,
                                       StateId = h.StateId,
                                        StateName = h.StateName,
                                         UserId = h.UserId,
                                          UserName = h.UserName
                    }
                    }
                });

                if(received.IsError)
                {
                    MessageBox.Show(received.ErrorDescription);
                    return;
                }
                MessageBox.Show("House has been successfully saved.");

                DTOHouse retHouse = received.RequestResponse.FirstOrDefault() as DTOHouse;
                h.Id = retHouse.Id;
                /////////////////////////////
                /////////////////////////////
                /////kada se iz ove forme dodaje kuca uredjaji nisu dodati, vec samo objekat kuce u glavnu bazu
                /////ako su dodaju i uredjaji onda se kasnije prelazi u edit house formu
                /////////////////////////////
                /////////////////////////////
                //List<Device> devices = new List<Device>();
                //foreach (var d in lbxDevices.Items)
                //{
                //    devices.Add(d as Device);
                //}

                //List<DTODevice> Devs = new List<DTODevice>();
                //foreach(var d in h.Devices)
                //{
                //    var actions = new List<DTOAction>();
                //    foreach(var a in d.Actions)
                //    {
                //        actions.Add(new DTOAction()
                //        {
                //            Description = a.Description,
                //            DeviceId = 0,
                //            IsRepeatable = a.IsRepeatable,
                //            MeasureUnit = a.MeasurementUnit,
                //            Name = a.Name,
                //            Id = string.IsNullOrEmpty(a.Id) ? 0 : int.Parse(a.Id)
                //        });
                //    }
                //    Devs.Add(new DTODevice()
                //    {
                //        ActionList = actions.ToArray(),
                //        DateOfReg = DateTime.UtcNow,
                //         Id = 0,
                //          Name = d.Name,
                //           Type = d.Type,
                //            URL = ""
                //    });
                //}
                //MessageToSend mts = ((Start)Owner).DbClient.AddDevices(Devs.ToArray());
                //if (mts != null)
                //    MessageBox.Show(mts.Body);

                ((Start)Owner).Users.Remove(u);
                u.Houses.Add(h);
                ((Start)Owner).Users.Add(u);
                Form f = new IndexFrm();
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to cancel creating new house?"
                    , "Cancelling"
                    , MessageBoxButtons.OKCancel
                    , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                Form f = new IndexFrm();
                f.Owner = this.Owner;
                f.Show();
                this.Dispose();
            }
        }

        private void lbxDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            //nebitno
        }
    }
}
