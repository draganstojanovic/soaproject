﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeAdmin.Data
{
    public class Device
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string DateOfReg { get; set; }
        /// <summary>
        /// kad se ucitava device samo se popuni sa name i id
        /// a kad se ide na add ili edit device tad se sve popunjava
        /// </summary>
        public List<Action> Actions { get; set; }

        public override bool Equals(object obj)
        {
            var device = obj as Device;
            return device != null && 
                (
                   Id == device.Id 
                   ||
                   Type == device.Type &&
                   Name == device.Name
                );
        }
    }
}
