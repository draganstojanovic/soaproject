﻿using Microsoft.AspNet.Identity;
using SmartHomeMain.DataLayer;
using SmartHomeMVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using UtilsAMQP.Models;
//using UtilsAMQP.Models;

namespace SmartHomeMVC.Helpers
{
    public class MessageHelper
    {
        public static MessageModel LogIn(MessageModel msgMdl)
        {
            using (var data = new ApplicationDbContext())
            {
                ApplicationUser user = data.Users.Where(x => x.Email.Equals(msgMdl.Email)).FirstOrDefault();
                if (user == null)
                    return new MessageModel() { IsError = true, ErrorDescription = "No such user!" };
                else
                {
                    PasswordHasher ph = new PasswordHasher();
                    if (ph.VerifyHashedPassword(user.PasswordHash, msgMdl.Password) != PasswordVerificationResult.Success)
                        return new MessageModel() { IsError = true, ErrorDescription = "Wrong password!" };
                    else
                        return new MessageModel() { IsError = false, IsResponse = true, RequestResponse = new List<object>() };
                }
            }
        }

        public static MessageModel ProcessMessageInstance(MessageModel obj)
        {
            if (obj.IsLogInMessage)
                return MessageHelper.LogIn(obj);

            if (!string.IsNullOrEmpty(obj.MethodName))
            {
                Type t = typeof(MessageHelper);
                MethodInfo mi = t.GetMethod(obj.MethodName);

                if (mi != null)
                {
                    MessageModel retObj = (MessageModel)mi.Invoke(new MessageHelper(), new object[] { obj });

                    return retObj;
                }
                else
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "Method with such name does not exist!" };
                }
            }

            return new MessageModel() { IsError = true, ErrorDescription = "Message format not correct (neither LogIn or MethodInvoker message)!" };
        }

        public MessageModel GetAllUsers(MessageModel gotMessage)
        {
            using (var data = new ApplicationDbContext())
            {
                var query = data.Users.Where(x => !x.Email.EndsWith("elfak.rs") && !x.Email.EndsWith("smarthome.rs"))
                    .Select(x => new DTOUser()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        Password = x.PasswordHash,
                        PhoneNumber = x.PhoneNumber,
                        UserName = x.UserName
                    });

                var users = query.ToList();

                MessageModel ret = new MessageModel() { RequestResponse = (users != null && users.Count > 0) ? new List<object>(users) : new List<object>() };

                return ret;
            }
        }

        public MessageModel GetAllStates(MessageModel gotMessage)
        {
            using (var data = new ApplicationDbContext())
            {
                var query = data.States
                    .Select(x => new DTOState()
                    {
                        Id = x.Id.ToString(),
                        Abbr = x.Abbrevation,
                        Name = x.Name
                    });

                var states = query.ToList();

                MessageModel ret = new MessageModel() { RequestResponse = (states != null && states.Count > 0) ? new List<object>(states) : new List<object>() };

                return ret;
            }
        }

        public MessageModel GetAllCities(MessageModel gotMessage)
        {
            using (var data = new ApplicationDbContext())
            {
                var query = data.Cities
                    .Select(x => new DTOCity()
                    {
                        Id = x.Id.ToString(),
                        Name = x.Name,
                        StateAbbr = x.State.Abbrevation,
                        StateId = x.StateId.ToString(),
                        StateName = x.State.Name
                    });

                var cities = query.ToList();

                MessageModel ret = new MessageModel() { RequestResponse = (cities != null && cities.Count > 0) ? new List<object>(cities) : new List<object>() };

                return ret;
            }
        }

        public MessageModel GetAllHousesForUser(MessageModel gotMessage)
        {
            using (var data = new ApplicationDbContext())
            {
                DTOUser user = null;
                try
                {
                    user = gotMessage.RequestResponse.FirstOrDefault() as DTOUser;
                    if (user == null)
                        throw new Exception("No user in list!");
                }
                catch (Exception e)
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "For the GetAllHousesForUser method the ResponseRequest list should contain the user in question (only that one in the first place in list).\nException: " + e.ToString() };
                }

                var query = from h in data.Houses.Where(x => x.UserId.Equals(user.Id))
                            join u in data.Users on h.UserId equals u.Id 
                            select new DTOHouse()
                            {
                                Id = h.Id.ToString(),
                                Address = h.Address,
                                CityId = h.CityId.ToString(),
                                CityName = h.City.Name,
                                Name = h.Name,
                                Contact = h.Contact,
                                DbName = h.DbName,
                                Ip = h.Ip,
                                Port = h.Port,
                                StateAbbr = h.State.Abbrevation,
                                StateId = h.StateId.ToString(),
                                StateName = h.State.Name,
                                UserId = u.Id,
                                UserName = u.UserName
                            };

                var houses = query.ToList();

                MessageModel ret = new MessageModel() { RequestResponse = (houses != null && houses.Count > 0) ? new List<object>(houses) : new List<object>() };

                return ret;
            }
        }

        public MessageModel AddHouse(MessageModel gotMessage)
        {
            DTOHouse house = null;
            try
            {
                house = gotMessage.RequestResponse.FirstOrDefault() as DTOHouse;
                if (house == null)
                    throw new Exception("No house in list!");
            }
            catch (Exception e)
            {
                return new MessageModel() { IsError = true, ErrorDescription = "For the AddHouse method the ResponseRequest list should contain the house in question (only that one in the first place in list).\nException: " + e.ToString() };
            }

            using (var data = new ApplicationDbContext())
            {
                House h = null;
                try
                {
                    h = new House()
                    {
                        Id = 0,
                        UserId = house.UserId,
                        Address = house.Address,
                        CityId = int.Parse(house.CityId),
                        Contact = house.Contact,
                        DbName = house.DbName,
                        Ip = house.Ip,
                        Name = house.Name,
                        Port = house.Port,
                        StateId = int.Parse(house.StateId)
                    };
                }
                catch (Exception e)
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "AddHouse method: The DTOHouse object in ResponseRequest list had some bad params.\nException: " + e.ToString() };
                }

                try
                {
                    //using (var conn = data.Database.Connection)
                    //{
                    //    DbCommand command = conn.CreateCommand();
                    //    command.CommandText = "INSERT INTO [SmartHomeMainDb].[dbo].[Houses]"
                    //        + " ([UserId], [Address], [CityId], [Contact], [DbName], [Ip], [Name], [Port], [StateId]) VALUES"
                    //        + " ('" + h.UserId + "', '" + h.Address + "', " + h.CityId + ", '" + h.Contact + "', '" + h.DbName + "', '"
                    //        + h.Ip + "', '" + h.Name + "', '" + h.Port + "', " + h.StateId + ")";
                    //    conn.Open();
                    //    command.ExecuteNonQuery();
                    //}
                    data.Houses.Add(h);
                    data.SaveChanges();
                }
                catch (Exception e)
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "AddHouse method: The DTOHouse object could not be added in the database.\nException: " + e.ToString() };
                }

                house.Id = h.Id.ToString();

                MessageModel ret = new MessageModel() { RequestResponse = new List<object>() { house } };
                return ret;
            }
        }
        public MessageModel UpdateHouse(MessageModel gotMessage)
        {
            DTOHouse house = null;
            try
            {
                house = gotMessage.RequestResponse.FirstOrDefault() as DTOHouse;
                if (house == null)
                    throw new Exception("No house in list!");
            }
            catch (Exception e)
            {
                return new MessageModel() { IsError = true, ErrorDescription = "For the AddHouse method the ResponseRequest list should contain the house in question (only that one in the first place in list).\nException: " + e.ToString() };
            }

            using (var data = new ApplicationDbContext())
            {
                House h = null;                

                try
                {
                    int houseId = int.Parse(house.Id);
                    int cityId = int.Parse(house.CityId),
                        stateId = int.Parse(house.StateId);

                    h = data.Houses.Where(x => x.Id == houseId).FirstOrDefault();

                    h.Address = house.Address;
                    h.CityId = cityId;
                    h.Contact = house.Contact;
                    h.DbName = house.DbName;
                    h.Ip = house.Ip;
                    h.Name = house.Name;
                    h.Port = house.Port;
                    h.StateId = stateId;

                    data.SaveChanges();
                }
                catch (Exception e)
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "AddHouse method: The DTOHouse object in ResponseRequest list had some bad params.\nException: " + e.ToString() };
                }

                try
                {
                    ApplicationUser user = data.Users.Where(x => x.Id.Equals(house.UserId)).FirstOrDefault();
                    house.UserName = user.UserName;

                    house.StateAbbr = h.State.Abbrevation;
                    house.StateName = h.State.Name;
                    house.CityName = h.City.Name;
                }
                catch (Exception) { }                

                MessageModel ret = new MessageModel() { RequestResponse = new List<object>() { house } };
                return ret;
            }
        }

        public MessageModel DeleteHouse(MessageModel gotMessage)
        {
            DTOHouse house = null;
            try
            {
                house = gotMessage.RequestResponse.FirstOrDefault() as DTOHouse;
                if (house == null)
                    throw new Exception("No house in list!");
            }
            catch (Exception e)
            {
                return new MessageModel() { IsError = true, ErrorDescription = "For the AddHouse method the ResponseRequest list should contain the house in question (only that one in the first place in list).\nException: " + e.ToString() };
            }

            using (var data = new ApplicationDbContext())
            {
                House h = null;

                try
                {
                    int houseId = int.Parse(house.Id);

                    try
                    {
                        h = data.Houses.Where(x => x.Id == houseId).FirstOrDefault();
                        data.Houses.Remove(h);
                        data.SaveChanges();
                    }
                    catch (Exception) { }
                }
                catch (Exception e)
                {
                    return new MessageModel() { IsError = true, ErrorDescription = "AddHouse method: The DTOHouse object in ResponseRequest list had some bad params.\nException: " + e.ToString() };
                }

                MessageModel ret = new MessageModel() { RequestResponse = new List<object>() { true } };
                return ret;
            }
        }
    }
}