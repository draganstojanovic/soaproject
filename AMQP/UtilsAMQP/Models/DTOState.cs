﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilsAMQP.Models
{
    public class DTOState : DTOBase
    {
        public string Name { get; set; }
        public string Abbr { get; set; }
    }
}
