﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartHomeMVC.Controllers
{
    public class ActionJobsController : Controller
    {
        // GET: ActionJobs
        public ActionResult Index(string targethost = "localhost")
        {
            ViewBag.TargetHost = targethost;
            return PartialView("Index");
        }
    }
}