﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilsAMQP.Models
{
    public class DTOHouse : DTOBase
    {
        public string Ip { get; set; }
        public string Port { get; set; }
        public string DbName { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string CityName { get; set; }
        public string CityId { get; set; }
        public string StateId { get; set; }
        public string StateName { get; set; }
        public string StateAbbr { get; set; }
    }
}
