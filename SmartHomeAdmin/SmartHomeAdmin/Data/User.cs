﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeAdmin.Data
{
    public class User
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// set na true kad se ucitaju kuce korisnika
        /// </summary>
        public bool Loaded { get; set; }

        public List<House> Houses { get; set; }

        public User()
        {
        }

        public override bool Equals(object obj)
        {
            var user = obj as User;
            return user != null &&
                   Id == user.Id &&
                   Email == user.Email &&
                   UserName == user.UserName;
        }
    }
}
