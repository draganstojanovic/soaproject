﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsAMQP.Models;

namespace SmartHomeAdmin
{
    public partial class LogIn : Form
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void LogIn_Load(object sender, EventArgs e)
        {
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(tbxUsername.Text) || string.IsNullOrEmpty(tbxPassword.Text))
            {
                lblError.Text = "Username or password is empty.";
                return;
            }
            //posalji username i password za login!!!

            MessageModel received = ((Start)Owner).Client.SendRecv(new MessageModel()
            {
                IsLogInMessage = true,
                Email = tbxUsername.Text,
                Password = tbxPassword.Text
            });

            //proveri gresku
            bool error = received.IsError;
            if(!error)
            {
                MessageModel cities = ((Start)Owner).Client.SendRecv(new MessageModel()
                {
                    MethodName = "GetAllCities"
                });

                if(cities.IsError)
                {
                    MessageBox.Show(cities.ErrorDescription);
                    return;
                }

                MessageModel states = ((Start)Owner).Client.SendRecv(new MessageModel()
                {
                    MethodName = "GetAllStates"
                });

                if (states.IsError)
                {
                    MessageBox.Show(states.ErrorDescription);
                    return;
                }
                MessageModel users = ((Start)Owner).Client.SendRecv(new MessageModel()
                {
                    MethodName = "GetAllUsers"
                });

                if (users.IsError)
                {
                    MessageBox.Show(users.ErrorDescription);
                    return;
                }

                ((Start)Owner).Users = new List<Data.User>();
                foreach (object u in users.RequestResponse)
                {
                    DTOUser user = u as DTOUser;
                    Data.User newUs = new Data.User()
                    {
                        Email = user.Email,
                        Houses = null,
                        Id = user.Id,
                        Loaded = false,
                        Password = user.Password,
                        PhoneNumber = user.PhoneNumber,
                        UserName = user.UserName
                    };
                    ((Start)Owner).Users.Add(newUs);
                }

                ((Start)Owner).ObjectCities.AllCities = new List<Data.City>();
                foreach (object u in cities.RequestResponse)
                {
                    DTOCity city = u as DTOCity;
                    Data.City newUs = new Data.City()
                    {
                        Id = city.Id,
                        Name = city.Name,
                        StateAbbr = city.StateAbbr,
                        StateName = city.StateName
                    };
                    ((Start)Owner).ObjectCities.AllCities.Add(newUs);
                }

                ((Start)Owner).ObjectStates.AllStates = new List<Data.State>();
                foreach (object u in states.RequestResponse)
                {
                    DTOState state = u as DTOState;
                    Data.State newUs = new Data.State()
                    {
                         Id = state.Id,
                          Name = state.Name,
                           Abbr = state.Abbr
                    };
                    ((Start)Owner).ObjectStates.AllStates.Add(newUs);
                }

                Form index = new IndexFrm();
                index.Owner = this.Owner;
                index.Show();
                this.Owner = null;
                this.Dispose();
            }
            else
            {
                MessageBox.Show(received.ErrorDescription);
            }
        }

        private void LogIn_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show(this.Owner,"Are you sure you would like to quit?","Quiting application"
                ,MessageBoxButtons.OKCancel,MessageBoxIcon.Exclamation) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void LogIn_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Owner.Dispose();
        }
    }
}
