﻿using SmartHomeAdmin.Data;
using SmartHomeAdmin.ServiceReference2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UtilsAMQP.Models;

namespace SmartHomeAdmin
{
    public partial class EditHouse : Form
    {
        House home;
        int selHouseIndex = -1;

        public EditHouse()
        {
            InitializeComponent();
        }

        public EditHouse(House h, int selectedHouseIndex) : this()
        {
            home = h;
            selHouseIndex = selectedHouseIndex;
        }

        public EditHouse(House h) : this()
        {
            this.home = h;
        }

        private void EditHouse_Load(object sender, EventArgs e)
        {
            //posalji zahtev wcf servisu za sve uredjaje

            tbxAddress.Text = home.Address;
            tbxContact.Text = home.Contact;
            tbxDbName.Text = home.DbName;
            tbxName.Text = home.Name;
            tbxServer.Text = home.Ip + ":"+home.Port;
            var cities = ((Start)Owner).ObjectCities.AllCities;
            var states = ((Start)Owner).ObjectStates.AllStates;
            City c = new City()
            {
                Id = home.CityId,
                Name = home.CityName
            };
            State s = new State()
            {
                Id = home.StateId,
                Name = home.StateName,
                Abbr = home.StateAbbr
            };
            cbxCity.DisplayMember = "Name";
            cbxCity.ValueMember = "Id";
            cbxCity.DataSource = cities;
            cbxCity.SelectedIndex = cities.IndexOf(c);

            cbxState.DisplayMember = "Name";
            cbxState.ValueMember = "Id";
            cbxState.DataSource = states;
            cbxState.SelectedIndex = states.IndexOf(s);

            lbxDevices.DisplayMember = "Name";
            lbxDevices.ValueMember = "Id";
            lbxDevices.DataSource = home.Devices;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
            List<Device> devices = new List<Device>();
            foreach (var d in lbxDevices.Items)
            {
                devices.Add(d as Device);
            }
            City c = cbxCity.SelectedItem as City;
            State st = cbxState.SelectedItem as State;
            House h = new House()
            {
                UserName = home.UserName,
                UserId = home.UserId,
                Id = home.Id,
                Name = tbxName.Text,
                Ip = s[0],
                Port = s[1],
                Address = tbxAddress.Text,
                CityName = c != null ? c.Name : "",
                CityId = c != null ? c.Id : "",
                StateId = st != null ? st.Id : "",
                StateName = st != null ? st.Name : "",
                Contact = tbxContact.Text,
                DbName = tbxDbName.Text,
                Devices = devices
            };
            Form f = new AddDevice(h);
            f.Owner = this.Owner;
            f.Show();
            this.Owner = null;
            this.Dispose();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
            List<Device> devices = new List<Device>();
            foreach (var d in lbxDevices.Items)
            {
                devices.Add(d as Device);
            }
            City c = cbxCity.SelectedItem as City;
            State st = cbxState.SelectedItem as State;
            House h = new House()
            {
                UserName = home.UserName,
                UserId = home.UserId,
                Name = tbxName.Text,
                Id = home.Id,
                Ip = s[0],
                Port = s[1],
                Address = tbxAddress.Text,
                CityName = c != null ? c.Name : "",
                CityId = c != null ? c.Id : "",
                StateId = st != null ? st.Id : "",
                StateName = st != null ? st.Name : "",
                Contact = tbxContact.Text,
                DbName = tbxDbName.Text,
                Devices = devices
            };
            Device dev = lbxDevices.SelectedItem as Device;
            Form f = new EditDevice(h, dev, lbxDevices.SelectedIndex);
            f.Owner = this.Owner;
            f.Show();
            this.Owner = null;
            this.Dispose();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lbxDevices.SelectedIndex == -1)
                return;
            Device d = lbxDevices.SelectedItem as Device;
            if (d == null)
                MessageBox.Show("Device does not exist!");
            home.Devices.RemoveAt(lbxDevices.SelectedIndex);
            //lbxDevices.Items.Remove(d);
            lbxDevices.DataSource = null;
            lbxDevices.DisplayMember = "Name";
            lbxDevices.ValueMember = "Id";
            lbxDevices.DataSource = home.Devices;
            lbxDevices.Refresh();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to submit?"
                    , "Submitting application"
                    , MessageBoxButtons.OKCancel
                    , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                //posalji house glavnoj bazi za edit, pa onda house devices lokalnoj za edit ili add
                User u = ((Start)Owner).Users.Where(x => x.Id.Equals(home.UserId)).FirstOrDefault();
                if (u == null)
                    return;
                string[] s = string.IsNullOrEmpty(tbxServer.Text) ? new string[] { "", "" } : tbxServer.Text.Split(':');
                //List<Device> devices = new List<Device>();
                //foreach (var d in lbxDevices.Items)
                //{
                //    devices.Add(d as Device);
                //}
                City c = cbxCity.SelectedItem as City;
                State st = cbxState.SelectedItem as State;
                House h = new House()
                {
                    UserName = home.UserName,
                    UserId = home.UserId,
                    Id = home.Id,
                    Name = tbxName.Text,
                    Ip = s[0],
                    Port = s[1],
                    Address = tbxAddress.Text,
                    CityName = c!=null ? c.Name : "",
                    CityId = c!=null ? c.Id : "",
                    StateId = st!=null ? st.Id : "",
                    StateName = st!=null ? st.Name : "",
                    Contact = tbxContact.Text,
                    DbName = tbxDbName.Text,
                    Devices = home.Devices
                };
                MessageModel received = null;
                //add house and devices
                if (h.Id == null || h.Id == "0")
                {
                    received = ((Start)Owner).Client.SendRecv(new MessageModel()
                    {
                        MethodName = "AddHouse",
                        RequestResponse = new List<object>()
                        { new DTOHouse()
                        {
                             Ip = h.Ip,
                              Name = h.Name,
                               Id = "",
                                Address = h.Address,
                                 CityId = h.CityId,
                                  CityName = h.CityName,
                                   Contact = h.Contact,
                                    DbName = h.DbName,
                                     Port = h.Port,
                                      StateAbbr = h.StateAbbr,
                                       StateId = h.StateId,
                                        StateName = h.StateName,
                                         UserId = h.UserId,
                                          UserName = h.UserName
                        }
                        }
                    });

                    if (received.IsError)
                    {
                        MessageBox.Show(received.ErrorDescription);
                        return;
                    }

                    DTOHouse retHouse = received.RequestResponse.FirstOrDefault() as DTOHouse;
                    if(retHouse==null || retHouse.Id==null || retHouse.Id=="0")
                    {
                        MessageBox.Show("Something went wront!");
                        return;
                    }
                    h.Id = retHouse.Id;

                    List<DTODevice> Devs = new List<DTODevice>();
                    foreach (var d in home.Devices)
                    {
                        var actions = new List<DTOAction>();
                        foreach (var a in d.Actions)
                        {
                            actions.Add(new DTOAction()
                            {
                                Description = a.Description,
                                DeviceId = 0,
                                IsRepeatable = a.IsRepeatable,
                                MeasureUnit = a.MeasurementUnit,
                                Name = a.Name,
                                Id = string.IsNullOrEmpty(a.Id) ? 0 : int.Parse(a.Id)
                            });
                        }
                        Devs.Add(new DTODevice()
                        {
                            ActionList = actions.ToArray(),
                            DateOfReg = DateTime.UtcNow,
                            Id = 0,
                            Name = d.Name,
                            Type = d.Type,
                            URL = ""
                        });
                    }
                    MessageToSend mts = ((Start)Owner).DbClient.AddDevices(Devs.ToArray());
                    if (mts == null)
                    {
                        MessageBox.Show("Upss. Something went wrong.");
                        return;
                    }
                    if(mts.IsError)
                    {
                        MessageBox.Show("There has been a mistake in processing your request.");
                        return;
                    }
                    string[] devs = mts.Body.Split(':');
                    foreach(var dev in devs)
                    {
                        string[] d = dev.Split('-');
                        int ind = home.Devices.FindIndex(x => x.Name == d[1] && x.Type == d[0]);
                        if (ind >= 0)
                        {
                            Device device = home.Devices.ElementAt(ind);
                            device.Id = d[2];
                            home.Devices.RemoveAt(ind);
                            home.Devices.Add(device);
                        }
                    }
                }
                else
                {
                    received = ((Start)Owner).Client.SendRecv(new MessageModel()
                    {
                        MethodName = "UpdateHouse",
                        RequestResponse = new List<object>() { new DTOHouse()
                        {
                             Ip = h.Ip,
                              Name = h.Name,
                               Id = h.Id,
                                Address = h.Address,
                                 CityId = h.CityId,
                                  CityName = h.CityName,
                                   Contact = h.Contact,
                                    DbName = h.DbName,
                                     Port = h.Port,
                                      StateAbbr = h.StateAbbr,
                                       StateId = h.StateId,
                                        StateName = h.StateName,
                                         UserId = h.UserId,
                                          UserName = h.UserName
                    }
                    }
                    });

                    if (received.IsError)
                    {
                        MessageBox.Show(received.ErrorDescription);
                        return;
                    }

                    DTOHouse retHouse = received.RequestResponse.FirstOrDefault() as DTOHouse;
                    if (retHouse != null)
                    {
                        h.CityName = retHouse.CityName;
                        h.StateAbbr = retHouse.StateAbbr;
                        h.StateName = retHouse.StateName;
                    }

                    List<DTODevice> Devs = new List<DTODevice>();
                    foreach (var d in home.Devices)
                    {
                        var actions = new List<DTOAction>();
                        foreach (var a in d.Actions)
                        {
                            actions.Add(new DTOAction()
                            {
                                Description = a.Description,
                                DeviceId = 0,
                                IsRepeatable = a.IsRepeatable,
                                MeasureUnit = a.MeasurementUnit,
                                Name = a.Name,
                                Id = string.IsNullOrEmpty(a.Id) ? 0 : int.Parse(a.Id)
                            });
                        }
                        Devs.Add(new DTODevice()
                        {
                            ActionList = actions.ToArray(),
                            DateOfReg = DateTime.UtcNow,
                            Id = 0,
                            Name = d.Name,
                            Type = d.Type,
                            URL = ""
                        });
                    }
                    MessageToSend mts = ((Start)Owner).DbClient.UpdateDevices(Devs.ToArray());
                    if (mts == null)
                    {
                        MessageBox.Show("Upss. Something went wrong.");
                        return;
                    }
                    MessageBox.Show(mts.Body);
                    if (mts.IsError)
                    {
                        return;
                    }                   
                }

                ((Start)Owner).Users.Remove(u);
                //edit
                if (selHouseIndex != -1)
                    u.Houses.RemoveAt(selHouseIndex);
                else
                    u.Houses.Remove(h);
                u.Houses.Add(h);
                ((Start)Owner).Users.Add(u);
                Form f = new IndexFrm();
                f.Owner = this.Owner;
                f.Show();
                this.Dispose();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to cancel creating new house?"
                    , "Cancelling"
                    , MessageBoxButtons.OKCancel
                    , MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                Form f = new IndexFrm();
                f.Owner = this.Owner;
                f.Show();
                this.Owner = null;
                this.Dispose();
            }
        }

        private void lbxDevices_SelectedIndexChanged(object sender, EventArgs e)
        {
            //nebitno
        }

        private void EditHouse_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show(this.Owner, "Are you sure you would like to quit?", "Quiting application"
                    , MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.Cancel)
            {
                e.Cancel = true;
            }
        }

        private void EditHouse_FormClosed(object sender, FormClosedEventArgs e)
        {
            Owner.Dispose();
        }
    }
}