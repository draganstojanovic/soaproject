﻿using System.Configuration;

namespace VirtualDevice.ConfigurationExtension
{
    public class DeviceTypeElementsCollection : ConfigurationElementCollection
    {
        private static ConfigurationPropertyCollection m_properties;

        static DeviceTypeElementsCollection()
        {
            m_properties = new ConfigurationPropertyCollection();
        }

        public DeviceTypeElementsCollection()
        {
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DeviceTypeElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DeviceTypeElement)element).Type;
        }

        protected override string ElementName
        {
            get { return "deviceType"; }
        }

        public DeviceTypeElement this[int index]
        {
            get { return (DeviceTypeElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        new public DeviceTypeElement this[string type]
        {
            get { return (DeviceTypeElement)BaseGet(type); }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }
    }
}
