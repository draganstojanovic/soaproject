﻿using DTOLayer;
using System;
using System.Timers;

namespace VirtualDevice
{
    //singleton instead of utility class because of the reflection(it can't be used on type, only on instance)
    public sealed class DeviceActions
    {
        public DeviceState DevState { get; set; }
        private static DeviceActions _instance;
        private static object lockObj = new object();

        private Timer SensorMeasureTimer { get; set; }
        private int SensorMeasureJobId { get; set; }

        private DeviceActions() { }
        public static DeviceActions GetInstance
        {
            get
            {
                if (_instance == null)
                {
                    //I dont need lock, it is only for thread safety, but I only have one thread that will read from DeviceActions class
                    lock (lockObj)
                    {
                        if (_instance == null)
                            _instance = new DeviceActions();
                    }
                }
                return _instance;
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////
        ////////////
        ////////////helper methods
        ////////////
        ////////////////////////////////////////////////////////////////////////////////////
        private DTOReturnLog CreateSensorReturnLog(int jobId, double value)
        {
            DTOReturnLog rl = new DTOReturnLog();
            rl.JobId = jobId;
            rl.IsReturnLog = true;
            rl.IsSensor = true;
            rl.IsError = false;
            rl.ReturnValue = value.ToString();
            rl.ReturnType = "double";
            return rl;
        }

        ////////////////////////////////////////////////////////////////////////////////////
        ////////////
        ////////////all actions must return DTOReturnLog
        ////////////
        ////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// this method creates timer if it needs to measure something more than once; maybe creating another thread for measurements?
        /// </summary>
        /// <returns>double that represents sensor measure</returns>
        public DTOReturnLog SensorMeasure(int jobId, object time, object minutesRep, object secondsRep)
        {
            //time ne treba da bude parametar, on je samo u job bitan a ovde kad se okine f-ja samo su rep bitni
            int repInterval = 0;
            int? minutes = minutesRep as int?;
            if (minutes != null)
            {
                repInterval += (int)minutes * 60;
            }

            int? seconds = secondsRep as int?;
            if (seconds != null)
            {
                repInterval += (int)seconds;
            }

            if (SensorMeasureTimer != null)
            {
                SensorMeasureTimer.Stop();
                SensorMeasureTimer.Dispose();
            }

            if (repInterval != 0)
            {                
                SensorMeasureJobId = jobId;
                SensorMeasureTimer = new Timer();
                SensorMeasureTimer.Interval = repInterval * 1000;
                //SensorMeasureTimer.Enabled = true;
                SensorMeasureTimer.Elapsed += new ElapsedEventHandler(SensorMeasure);
                SensorMeasureTimer.Start();
            }

            return CreateSensorReturnLog(jobId, 5);
        }

        private void SensorMeasure(object sender, ElapsedEventArgs e)
        {
            Random rd = new Random((int)(DateTime.Now.Ticks % 10000));

            double rndVal = rd.NextDouble() * 100.0;

            Console.BackgroundColor = ConsoleColor.Gray;
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine(System.Reflection.MethodBase.GetCurrentMethod().Name);
            Console.ResetColor();

            DTOReturnLog rl = CreateSensorReturnLog(SensorMeasureJobId, rndVal);

            DevState.device.sc.SendMessage(rl);
        }

        public DTOReturnLog MeasureTemperature(int jobId, object time, object minutesRep, object secondsRep)
        {
            return CreateSensorReturnLog(jobId, 1);
        }

        public DTOReturnLog MeasurePressure(int jobId, object time, object minutesRep, object secondsRep)
        {
            return CreateSensorReturnLog(jobId, 2);
        }

        public DTOReturnLog MeasureHumidity(int jobId, object time, object minutesRep, object secondsRep)
        {
            return CreateSensorReturnLog(jobId, 3);
        }
    }
}
