﻿namespace SmartHomeAdmin
{
    partial class EditDevice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbllblHouseOwner = new System.Windows.Forms.Label();
            this.lblHouseOwner = new System.Windows.Forms.Label();
            this.lbllblHouseName = new System.Windows.Forms.Label();
            this.lblHouseName = new System.Windows.Forms.Label();
            this.lbllblDeviceType = new System.Windows.Forms.Label();
            this.lbllblDeviceName = new System.Windows.Forms.Label();
            this.tbxDeviceType = new System.Windows.Forms.TextBox();
            this.tbxDeviceName = new System.Windows.Forms.TextBox();
            this.lbxActions = new System.Windows.Forms.ListBox();
            this.lbllblDeviceActions = new System.Windows.Forms.Label();
            this.btnAddAction = new System.Windows.Forms.Button();
            this.btnEditAction = new System.Windows.Forms.Button();
            this.btnDeleteAction = new System.Windows.Forms.Button();
            this.lbllblName = new System.Windows.Forms.Label();
            this.lbllblDescription = new System.Windows.Forms.Label();
            this.lblRepeatable = new System.Windows.Forms.Label();
            this.lbllblMeasurementUnit = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lbllblActionManagement = new System.Windows.Forms.Label();
            this.tbxName = new System.Windows.Forms.TextBox();
            this.cbxRepeatable = new System.Windows.Forms.CheckBox();
            this.tbxDescription = new System.Windows.Forms.TextBox();
            this.tbxMeasurementUnit = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbllblHouseOwner
            // 
            this.lbllblHouseOwner.AutoSize = true;
            this.lbllblHouseOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblHouseOwner.Location = new System.Drawing.Point(41, 28);
            this.lbllblHouseOwner.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbllblHouseOwner.Name = "lbllblHouseOwner";
            this.lbllblHouseOwner.Size = new System.Drawing.Size(117, 20);
            this.lbllblHouseOwner.TabIndex = 0;
            this.lbllblHouseOwner.Text = "House Owner";
            // 
            // lblHouseOwner
            // 
            this.lblHouseOwner.AutoSize = true;
            this.lblHouseOwner.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouseOwner.Location = new System.Drawing.Point(41, 49);
            this.lblHouseOwner.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHouseOwner.Name = "lblHouseOwner";
            this.lblHouseOwner.Size = new System.Drawing.Size(60, 24);
            this.lblHouseOwner.TabIndex = 1;
            this.lblHouseOwner.Text = "label2";
            // 
            // lbllblHouseName
            // 
            this.lbllblHouseName.AutoSize = true;
            this.lbllblHouseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblHouseName.Location = new System.Drawing.Point(234, 28);
            this.lbllblHouseName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbllblHouseName.Name = "lbllblHouseName";
            this.lbllblHouseName.Size = new System.Drawing.Size(112, 20);
            this.lbllblHouseName.TabIndex = 2;
            this.lbllblHouseName.Text = "House Name";
            // 
            // lblHouseName
            // 
            this.lblHouseName.AutoSize = true;
            this.lblHouseName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHouseName.Location = new System.Drawing.Point(234, 48);
            this.lblHouseName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHouseName.Name = "lblHouseName";
            this.lblHouseName.Size = new System.Drawing.Size(60, 24);
            this.lblHouseName.TabIndex = 3;
            this.lblHouseName.Text = "label4";
            // 
            // lbllblDeviceType
            // 
            this.lbllblDeviceType.AutoSize = true;
            this.lbllblDeviceType.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblDeviceType.Location = new System.Drawing.Point(41, 104);
            this.lbllblDeviceType.Name = "lbllblDeviceType";
            this.lbllblDeviceType.Size = new System.Drawing.Size(106, 20);
            this.lbllblDeviceType.TabIndex = 4;
            this.lbllblDeviceType.Text = "Device Type";
            // 
            // lbllblDeviceName
            // 
            this.lbllblDeviceName.AutoSize = true;
            this.lbllblDeviceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblDeviceName.Location = new System.Drawing.Point(41, 132);
            this.lbllblDeviceName.Name = "lbllblDeviceName";
            this.lbllblDeviceName.Size = new System.Drawing.Size(114, 20);
            this.lbllblDeviceName.TabIndex = 5;
            this.lbllblDeviceName.Text = "Device Name";
            // 
            // tbxDeviceType
            // 
            this.tbxDeviceType.Location = new System.Drawing.Point(233, 102);
            this.tbxDeviceType.Name = "tbxDeviceType";
            this.tbxDeviceType.Size = new System.Drawing.Size(158, 22);
            this.tbxDeviceType.TabIndex = 6;
            // 
            // tbxDeviceName
            // 
            this.tbxDeviceName.Location = new System.Drawing.Point(233, 130);
            this.tbxDeviceName.Name = "tbxDeviceName";
            this.tbxDeviceName.Size = new System.Drawing.Size(158, 22);
            this.tbxDeviceName.TabIndex = 7;
            // 
            // lbxActions
            // 
            this.lbxActions.FormattingEnabled = true;
            this.lbxActions.ItemHeight = 16;
            this.lbxActions.Location = new System.Drawing.Point(434, 49);
            this.lbxActions.Name = "lbxActions";
            this.lbxActions.Size = new System.Drawing.Size(240, 372);
            this.lbxActions.TabIndex = 8;
            // 
            // lbllblDeviceActions
            // 
            this.lbllblDeviceActions.AutoSize = true;
            this.lbllblDeviceActions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblDeviceActions.Location = new System.Drawing.Point(430, 26);
            this.lbllblDeviceActions.Name = "lbllblDeviceActions";
            this.lbllblDeviceActions.Size = new System.Drawing.Size(128, 20);
            this.lbllblDeviceActions.TabIndex = 9;
            this.lbllblDeviceActions.Text = "Device Actions";
            // 
            // btnAddAction
            // 
            this.btnAddAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAction.Location = new System.Drawing.Point(45, 357);
            this.btnAddAction.Name = "btnAddAction";
            this.btnAddAction.Size = new System.Drawing.Size(110, 23);
            this.btnAddAction.TabIndex = 10;
            this.btnAddAction.Text = "Add Action";
            this.btnAddAction.UseVisualStyleBackColor = true;
            this.btnAddAction.Click += new System.EventHandler(this.btnAddAction_Click);
            // 
            // btnEditAction
            // 
            this.btnEditAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAction.Location = new System.Drawing.Point(45, 398);
            this.btnEditAction.Name = "btnEditAction";
            this.btnEditAction.Size = new System.Drawing.Size(109, 23);
            this.btnEditAction.TabIndex = 11;
            this.btnEditAction.Text = "Edit Action";
            this.btnEditAction.UseVisualStyleBackColor = true;
            this.btnEditAction.Click += new System.EventHandler(this.btnEditAction_Click);
            // 
            // btnDeleteAction
            // 
            this.btnDeleteAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAction.Location = new System.Drawing.Point(165, 357);
            this.btnDeleteAction.Name = "btnDeleteAction";
            this.btnDeleteAction.Size = new System.Drawing.Size(112, 23);
            this.btnDeleteAction.TabIndex = 12;
            this.btnDeleteAction.Text = "Delete Action";
            this.btnDeleteAction.UseVisualStyleBackColor = true;
            this.btnDeleteAction.Click += new System.EventHandler(this.btnDeleteAction_Click);
            // 
            // lbllblName
            // 
            this.lbllblName.AutoSize = true;
            this.lbllblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblName.Location = new System.Drawing.Point(41, 214);
            this.lbllblName.Name = "lbllblName";
            this.lbllblName.Size = new System.Drawing.Size(55, 20);
            this.lbllblName.TabIndex = 13;
            this.lbllblName.Text = "Name";
            // 
            // lbllblDescription
            // 
            this.lbllblDescription.AutoSize = true;
            this.lbllblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblDescription.Location = new System.Drawing.Point(41, 253);
            this.lbllblDescription.Name = "lbllblDescription";
            this.lbllblDescription.Size = new System.Drawing.Size(100, 20);
            this.lbllblDescription.TabIndex = 14;
            this.lbllblDescription.Text = "Description";
            // 
            // lblRepeatable
            // 
            this.lblRepeatable.AutoSize = true;
            this.lblRepeatable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRepeatable.Location = new System.Drawing.Point(41, 321);
            this.lblRepeatable.Name = "lblRepeatable";
            this.lblRepeatable.Size = new System.Drawing.Size(102, 20);
            this.lblRepeatable.TabIndex = 15;
            this.lblRepeatable.Text = "Repeatable";
            // 
            // lbllblMeasurementUnit
            // 
            this.lbllblMeasurementUnit.AutoSize = true;
            this.lbllblMeasurementUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblMeasurementUnit.Location = new System.Drawing.Point(41, 292);
            this.lbllblMeasurementUnit.Name = "lbllblMeasurementUnit";
            this.lbllblMeasurementUnit.Size = new System.Drawing.Size(156, 20);
            this.lbllblMeasurementUnit.TabIndex = 16;
            this.lbllblMeasurementUnit.Text = "Measurement Unit";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Location = new System.Drawing.Point(293, 357);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(117, 23);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubmit.Location = new System.Drawing.Point(293, 398);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(117, 23);
            this.btnSubmit.TabIndex = 18;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lbllblActionManagement
            // 
            this.lbllblActionManagement.AutoSize = true;
            this.lbllblActionManagement.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllblActionManagement.Location = new System.Drawing.Point(41, 184);
            this.lbllblActionManagement.Name = "lbllblActionManagement";
            this.lbllblActionManagement.Size = new System.Drawing.Size(169, 20);
            this.lbllblActionManagement.TabIndex = 19;
            this.lbllblActionManagement.Text = "Action Management";
            // 
            // tbxName
            // 
            this.tbxName.Location = new System.Drawing.Point(233, 214);
            this.tbxName.Name = "tbxName";
            this.tbxName.Size = new System.Drawing.Size(158, 22);
            this.tbxName.TabIndex = 20;
            // 
            // cbxRepeatable
            // 
            this.cbxRepeatable.AutoSize = true;
            this.cbxRepeatable.Location = new System.Drawing.Point(376, 327);
            this.cbxRepeatable.Name = "cbxRepeatable";
            this.cbxRepeatable.Size = new System.Drawing.Size(15, 14);
            this.cbxRepeatable.TabIndex = 21;
            this.cbxRepeatable.UseVisualStyleBackColor = true;
            // 
            // tbxDescription
            // 
            this.tbxDescription.Location = new System.Drawing.Point(233, 253);
            this.tbxDescription.Name = "tbxDescription";
            this.tbxDescription.Size = new System.Drawing.Size(158, 22);
            this.tbxDescription.TabIndex = 22;
            // 
            // tbxMeasurementUnit
            // 
            this.tbxMeasurementUnit.Location = new System.Drawing.Point(233, 292);
            this.tbxMeasurementUnit.Name = "tbxMeasurementUnit";
            this.tbxMeasurementUnit.Size = new System.Drawing.Size(158, 22);
            this.tbxMeasurementUnit.TabIndex = 23;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(169, 398);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 23);
            this.btnSave.TabIndex = 24;
            this.btnSave.Text = "Save Action";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // EditDevice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tbxMeasurementUnit);
            this.Controls.Add(this.tbxDescription);
            this.Controls.Add(this.cbxRepeatable);
            this.Controls.Add(this.tbxName);
            this.Controls.Add(this.lbllblActionManagement);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lbllblMeasurementUnit);
            this.Controls.Add(this.lblRepeatable);
            this.Controls.Add(this.lbllblDescription);
            this.Controls.Add(this.lbllblName);
            this.Controls.Add(this.btnDeleteAction);
            this.Controls.Add(this.btnEditAction);
            this.Controls.Add(this.btnAddAction);
            this.Controls.Add(this.lbllblDeviceActions);
            this.Controls.Add(this.lbxActions);
            this.Controls.Add(this.tbxDeviceName);
            this.Controls.Add(this.tbxDeviceType);
            this.Controls.Add(this.lbllblDeviceName);
            this.Controls.Add(this.lbllblDeviceType);
            this.Controls.Add(this.lblHouseName);
            this.Controls.Add(this.lbllblHouseName);
            this.Controls.Add(this.lblHouseOwner);
            this.Controls.Add(this.lbllblHouseOwner);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "EditDevice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.EditDevice_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbllblHouseOwner;
        private System.Windows.Forms.Label lblHouseOwner;
        private System.Windows.Forms.Label lbllblHouseName;
        private System.Windows.Forms.Label lblHouseName;
        private System.Windows.Forms.Label lbllblDeviceType;
        private System.Windows.Forms.Label lbllblDeviceName;
        private System.Windows.Forms.TextBox tbxDeviceType;
        private System.Windows.Forms.TextBox tbxDeviceName;
        private System.Windows.Forms.ListBox lbxActions;
        private System.Windows.Forms.Label lbllblDeviceActions;
        private System.Windows.Forms.Button btnAddAction;
        private System.Windows.Forms.Button btnEditAction;
        private System.Windows.Forms.Button btnDeleteAction;
        private System.Windows.Forms.Label lbllblName;
        private System.Windows.Forms.Label lbllblDescription;
        private System.Windows.Forms.Label lblRepeatable;
        private System.Windows.Forms.Label lbllblMeasurementUnit;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lbllblActionManagement;
        private System.Windows.Forms.TextBox tbxName;
        private System.Windows.Forms.CheckBox cbxRepeatable;
        private System.Windows.Forms.TextBox tbxDescription;
        private System.Windows.Forms.TextBox tbxMeasurementUnit;
        private System.Windows.Forms.Button btnSave;
    }
}